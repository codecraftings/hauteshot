// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;

namespace HauteShot.iOS
{
	[Register ("SignupViewController")]
	partial class SignupViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField Email { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel ErrorContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HTHeaderView Header { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField Name { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField Password { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView ScrollContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton SignupButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField Username { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (Email != null) {
				Email.Dispose ();
				Email = null;
			}
			if (ErrorContainer != null) {
				ErrorContainer.Dispose ();
				ErrorContainer = null;
			}
			if (Header != null) {
				Header.Dispose ();
				Header = null;
			}
			if (Name != null) {
				Name.Dispose ();
				Name = null;
			}
			if (Password != null) {
				Password.Dispose ();
				Password = null;
			}
			if (ScrollContainer != null) {
				ScrollContainer.Dispose ();
				ScrollContainer = null;
			}
			if (SignupButton != null) {
				SignupButton.Dispose ();
				SignupButton = null;
			}
			if (Username != null) {
				Username.Dispose ();
				Username = null;
			}
		}
	}
}
