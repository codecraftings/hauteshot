using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;
using CoreGraphics;

namespace HauteShot.iOS
{
	partial class SignupViewController : BaseUIController
	{
		public new SignupViewModel ViewModel {
			get {
				return (SignupViewModel)base.ViewModel;
			}
			set {
				base.ViewModel = (BaseViewModel)value;
			}
		}

		public SignupViewController (IntPtr handle) : base (handle)
		{
		}

		public void KeyboardVisible (NSNotification notification)
		{
			CGSize keyboardInfo = UIKeyboard.FrameEndFromNotification (notification).Size;
			var inset = new UIEdgeInsets (0, 0, keyboardInfo.Height, 0);
			ScrollContainer.ContentInset = inset;
			ScrollContainer.ScrollIndicatorInsets = inset;
			var activeField = ScrollContainer.FindFirstResponder ();
			if (activeField != null)
				ScrollContainer.ScrollRectToVisible (activeField.Frame, true);
		}

		public void KeyboardHidden (NSNotification notification)
		{
			ScrollContainer.ContentInset = UIEdgeInsets.Zero;
			ScrollContainer.ScrollIndicatorInsets = UIEdgeInsets.Zero;
		}

		protected void DismissKeyboardOnBackgroundTap ()
		{
			// Add gesture recognizer to hide keyboard
			var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
			tap.AddTarget (() => View.EndEditing (true));
			View.AddGestureRecognizer (tap);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			ViewModel = new SignupViewModel ();
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, KeyboardVisible);
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, KeyboardHidden);
			DismissKeyboardOnBackgroundTap ();
			Header.BackButton.TouchUpInside += (object sender, EventArgs e) => NavigationController.PopViewController (true);
			Name.ShouldReturn += (textField) => {
				textField.ResignFirstResponder ();
				Email.BecomeFirstResponder ();
				return false;
			};
			Email.ShouldReturn += (textField) => {
				textField.ResignFirstResponder ();
				Username.BecomeFirstResponder();
				return false;
			};
			Username.ShouldReturn += (textField) => {
				textField.ResignFirstResponder ();
				Password.BecomeFirstResponder ();
				return false;
			};
			Password.ShouldReturn += (textField) => textField.ResignFirstResponder ();
			SignupButton.TouchUpInside += async (object sender, EventArgs e) => {
				ShowUIBusy (this.View);
				ErrorContainer.Hidden = true;
				ViewModel.Name = Name.Text;
				ViewModel.Email = Email.Text;
				ViewModel.Username = Username.Text;
				ViewModel.Password = Password.Text;
				if (await ViewModel.TrySignup ()) {
					var view = Storyboard.InstantiateViewController ("HomeFeedView") as BaseUIController;
					if (view != null) {
						NavigationController.PushViewController (view, true);
					}
				}
				ErrorContainer.Text = ViewModel.Error.Message;
				ErrorContainer.Hidden = false;
				HideUIBusy (this.View);
			};
		}
	}
}
