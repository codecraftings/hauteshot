using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;
using System.Threading.Tasks;
using CoreGraphics;

namespace HauteShot.iOS
{
	partial class LoginViewController : BaseUIController
	{
		//private NSObject observer1;
		//private NSObject observer2;
		public new LoginViewModel ViewModel {
			get {
				return (LoginViewModel)base.ViewModel;
			}
			set {
				base.ViewModel = (BaseViewModel)value;
			}
		}

		public LoginViewController (IntPtr handle) : base (handle)
		{

		}

		public void KeyboardVisible (NSNotification notification)
		{
			CGSize keyboardInfo = UIKeyboard.FrameEndFromNotification (notification).Size;
			var inset = new UIEdgeInsets (0, 0, keyboardInfo.Height, 0);
			ScrollContainer.ContentInset = inset;
			ScrollContainer.ScrollIndicatorInsets = inset;
			var activeField = ScrollContainer.FindFirstResponder ();
			if(activeField!=null)
			ScrollContainer.ScrollRectToVisible (activeField.Frame, true);
		}

		public void KeyboardHidden (NSNotification notification)
		{
			ScrollContainer.ContentInset = UIEdgeInsets.Zero;
			ScrollContainer.ScrollIndicatorInsets = UIEdgeInsets.Zero;
		}

		protected void DismissKeyboardOnBackgroundTap ()
		{
			// Add gesture recognizer to hide keyboard
			var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
			tap.AddTarget (() => View.EndEditing (true));
			View.AddGestureRecognizer (tap);
		}
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			//NSNotificationCenter.DefaultCenter.RemoveObserver (observer1);
			//NSNotificationCenter.DefaultCenter.RemoveObserver (observer2);
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.ViewModel = new LoginViewModel ();
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, KeyboardVisible);
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, KeyboardHidden);
			DismissKeyboardOnBackgroundTap ();
			UsernameText.ShouldReturn = (UITextField textField) => {
				textField.ResignFirstResponder ();
				PasswordText.BecomeFirstResponder ();
				return false;
			};
			PasswordText.ShouldReturn = (textField) => {
				textField.ResignFirstResponder ();
				return false;
			};
			LoginButton.TouchUpInside += async (object sender, EventArgs e) => {
				LoginButton.Enabled = false;
				LoadingView.StartAnimating ();
				NoticeLabel.Hidden = true;
				await Task.Delay (1000);
				PasswordText.ResignFirstResponder ();
				UsernameText.ResignFirstResponder ();
				ViewModel.UserName = UsernameText.Text;
				ViewModel.Password = PasswordText.Text;
				if (ViewModel.TryLogin ()) {
					var view = Storyboard.InstantiateViewController ("HomeFeedView") as BaseUIController;
					if (view != null) {
						NavigationController.PushViewController (view, true);
					}
				}
				LoadingView.StopAnimating ();
				NoticeLabel.Text = ViewModel.Error;
				NoticeLabel.Hidden = false;
				LoginButton.Enabled = true;
			};
			ToSignup.TouchUpInside += (object sender, EventArgs e) => {
				var view = Storyboard.InstantiateViewController ("SignupView") as BaseUIController;
				NavigationController.PushViewController (view, true);
			};
		}
	}
}
