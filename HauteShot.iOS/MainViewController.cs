using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;
using System.Net;

namespace HauteShot.iOS
{
	partial class MainViewController : BaseUIController
	{
		IUserRepo userRepo;
		public MainViewController (IntPtr handle) : base (handle)
		{
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			userRepo = ServiceContainer.Resolve<IUserRepo> ();
			if (!userRepo.IsLoggedIn()) {
				var view = Storyboard.InstantiateViewController("LoginView") as BaseUIController;
				if(view!=null){
					NavigationController.PushViewController(view, true);
				}
			} else {
				var view = Storyboard.InstantiateViewController("HomeFeedView") as BaseUIController;
				if(view!=null){
					NavigationController.PushViewController(view, true);
				}
			}
		}
	}
}
