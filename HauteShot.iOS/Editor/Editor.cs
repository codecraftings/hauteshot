﻿using System;

using CoreGraphics;
using CoreImage;
using UIKit;
using System.Threading.Tasks;

namespace HauteShot.iOS
{
	public class Editor
	{
		public UIImage OriginalImage;
		public UIImage EditedImage;
		private CIContext context;
		private UIImagePickerController picker;
		private UINavigationController navigationController;
		private static Editor _instance;
		private System.Threading.CancellationTokenSource cts;
		private CIImage ciImage;
		private UIImage editedImage;
		private CIPhotoEffect effect;
		private string appliedEffect;
		public static Editor Instance {
			get {
				if (_instance == null) {
					_instance = new Editor ();
				}
				return _instance;
			}
		}

		public static string[] AvailableEffects {
			get {
				return new string[]{ "none","chrome","fade","instant","mono","noir","process","tonal","transfer" };
			}
		}

		public Editor ()
		{
		}
		public async void ApplyEffectAsync(string effectName, Action<UIImage> onComplete, nfloat newW, nfloat newH){
			if (cts != null) {
				cts.Cancel ();
			}
			cts = new System.Threading.CancellationTokenSource ();
			await Task.Run(() => {
				ApplyEffect(effectName, onComplete, newW, newH, cts.Token);
			});
		}
		public void ApplyEffect (string effectName, Action<UIImage> onComplete, nfloat newW, nfloat newH, System.Threading.CancellationToken ctsToken)
		{
			ciImage = CIImage.FromCGImage (OriginalImage.CGImage);
			appliedEffect = effectName;
			switch (effectName) {
			case "chrome":
				effect = new CIPhotoEffectChrome ();
				break;
			case "noir":
				effect = new CIPhotoEffectNoir ();
				break;
			case "fade":
				effect = new CIPhotoEffectFade ();
				break;
			case "instant":
				effect = new CIPhotoEffectInstant ();
				break;
			case "mono":
				effect = new CIPhotoEffectMono ();
				break;
			case "process":
				effect = new CIPhotoEffectProcess ();
				break;
			case "tonal":
				effect = new CIPhotoEffectTonal ();
				break;
			case "transfer":
				effect = new CIPhotoEffectTransfer ();
				break;
			default:
				effect = null;
				break;
			}
			if (effect != null) {
				effect.Image = ciImage;
				ciImage = effect.OutputImage;
			}
			if (context == null) {
				context = CIContext.FromOptions (null);
			}
			if(newH==0)
				newH = newW * OriginalImage.Size.Height / OriginalImage.Size.Width;
			if(newW==0)
				newW = newH * OriginalImage.Size.Width / OriginalImage.Size.Height;
			if (ctsToken.IsCancellationRequested) {
				return;
			}
			ciImage = ciImage.ImageByApplyingTransform (CGAffineTransform.MakeScale (newW/OriginalImage.Size.Width, newH/OriginalImage.Size.Height));
			if(OriginalImage.Size.Height>OriginalImage.Size.Width)
			ciImage = ciImage.ImageByApplyingTransform (CGAffineTransform.MakeRotation((nfloat)(Math.PI*0.5*(-1))));
			if (ctsToken.IsCancellationRequested) {
				return;
			}
			UIImage editedImage = UIImage.FromImage (context.CreateCGImage(ciImage, ciImage.Extent));
			if (ctsToken.IsCancellationRequested) {
				return;
			}
			navigationController.InvokeOnMainThread (() => {
				Console.WriteLine(effectName);
				onComplete (editedImage);
			});
		}
		public async void GetFinalImage(Action<UIImage> onDone){
			if (cts != null) {
				cts.Cancel ();
			}
			cts = new System.Threading.CancellationTokenSource ();
			await Task.Run (() => {
				ApplyEffect (appliedEffect, onDone, 800, 0, cts.Token);
			});
		}
		private void initPicker ()
		{
			this.picker = new UIImagePickerController ();
			picker.FinishedPickingMedia += HandleFinishedPickingMedia;
			picker.Canceled += HandlePickerCancelled;
		}

		void HandlePickerCancelled (object sender, EventArgs e)
		{
			Console.WriteLine ("cancelled");
			picker.DismissViewController (true, null);
			this.FreeUp ();
		}

		void HandleFinishedPickingMedia (object sender, UIImagePickerMediaPickedEventArgs e)
		{
			OriginalImage = e.OriginalImage;
			//Console.WriteLine ((e.Info as MonoTouch.Foundation.NSDictionary).Description);
			picker.DismissViewController (false, null);
			navigationController.PushViewController (navigationController.Storyboard.InstantiateViewController ("EditorView") as EditorViewController, false);
		}

		public void CameraPicker (UINavigationController navigationController)
		{
			this.navigationController = navigationController;
			initPicker ();
			picker.SourceType = UIImagePickerControllerSourceType.Camera;
			navigationController.PresentViewController (picker, true, null);
		}

		public void PhotoPicker (UINavigationController navigationController)
		{
			this.navigationController = navigationController;
			initPicker ();
			picker.SourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum;
			navigationController.PresentViewController (picker, true, null);
		}
		public bool HasCamera(){
			return UIImagePickerController.IsSourceTypeAvailable (UIImagePickerControllerSourceType.Camera);
		}
		public void FreeUp ()
		{
			if(effect!=null)
				effect.Dispose ();
			if(ciImage!=null)
				ciImage.Dispose ();
			if(editedImage!=null)
				editedImage.Dispose ();
			if(context!=null)
				this.context.Dispose ();
			if(OriginalImage!=null)
				this.OriginalImage.Dispose ();
			if(picker!=null)
				this.picker.Dispose ();
			appliedEffect = null;
			this.OriginalImage = null;
			this.context = null;
			this.picker = null;
		}
	}
}

