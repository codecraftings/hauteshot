using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;
using System.IO;

namespace HauteShot.iOS
{
	partial class EditorViewController : BaseUIController
	{
		private bool uploading;
		private bool doneDisabled;

		public new EditorViewModel ViewModel {
			get {
				return (EditorViewModel)base.ViewModel;
			}
			set {
				base.ViewModel = value as EditorViewModel;
			}
		}

		public EditorViewController (IntPtr handle) : base (handle)
		{
			uploading = false;
			doneDisabled = false;
			this.ViewModel = new EditorViewModel (this);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			if (Editor.Instance.OriginalImage != null) {
				ImageHolder.Image = Editor.Instance.OriginalImage;
			}
			EffectsContainer.OnEffectSelected += (string effectId) => {
				if (uploading) {
					return;
				}
				doneDisabled = true;
				ShowUIBusy (ImageHolder, UIColor.Black, UIColor.White, 30, 30);
				ImageHolder.Layer.Opacity = .3f;
				Editor.Instance.ApplyEffectAsync (effectId, (UIImage output) => {
					ImageHolder.Image = output;
					HideUIBusy (ImageHolder);
					ImageHolder.Layer.Opacity = 1f;
				}, 0, ImageHolder.Bounds.Height);
				doneDisabled = false;
			};
			CancelButton.TouchUpInside += (object sender, EventArgs e) => {
				if (uploading) {
					return;
				}
				Editor.Instance.FreeUp ();
				NavigationController.PopViewController (true);
			};
			DoneButton.TouchUpInside += (object sender, EventArgs e) => {
				if (doneDisabled) {
					return;
				}
				uploading = true;
				UIView.Animate(.4, ()=>{
					UploadingStatus.Hidden = false;
				});
				ShowUIBusy (UploadingStatus);
				StsMsg.Text = "Initializing..";
				Editor.Instance.GetFinalImage ((img) => {
					StsMsg.Text = "Saving..";
					var dir = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
					var path = System.IO.Path.Combine (dir, System.Guid.NewGuid ().ToString () + ".png");
					NSData data = img.AsPNG ();
					data.Save (path, true);
					StsMsg.Text = "Uploading..";
					ViewModel.UploadImage (path, (i) => {
						InvokeOnMainThread(()=>{
							StsMsg.Text = "Uploading.."+(i*100).ToString()+"%";
						});
					}, photo => {
						InvokeOnMainThread (() => {
							var view = Storyboard.InstantiateViewController ("PhotoDetailViewer") as PhotoViewController;
							view.PhotoID = photo.id;
							view.BackButtonEnabled = false;
							if (NavigationController != null) {
								NavigationController.PushViewController (view, true);
							}
						});
					});
				});
			};
		}
	}
}
