// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;

namespace HauteShot.iOS
{
	[Register ("EditorViewController")]
	partial class EditorViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView ButtonsContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton CancelButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton DoneButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		EffectsPickerView EffectsContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HTHeaderView Header { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView ImageHolder { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel StsMsg { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView UploadingStatus { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ButtonsContainer != null) {
				ButtonsContainer.Dispose ();
				ButtonsContainer = null;
			}
			if (CancelButton != null) {
				CancelButton.Dispose ();
				CancelButton = null;
			}
			if (DoneButton != null) {
				DoneButton.Dispose ();
				DoneButton = null;
			}
			if (EffectsContainer != null) {
				EffectsContainer.Dispose ();
				EffectsContainer = null;
			}
			if (Header != null) {
				Header.Dispose ();
				Header = null;
			}
			if (ImageHolder != null) {
				ImageHolder.Dispose ();
				ImageHolder = null;
			}
			if (StsMsg != null) {
				StsMsg.Dispose ();
				StsMsg = null;
			}
			if (UploadingStatus != null) {
				UploadingStatus.Dispose ();
				UploadingStatus = null;
			}
		}
	}
}
