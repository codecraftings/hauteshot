﻿using System;
using HauteShot.Core;

namespace HauteShot.iOS
{
	public class HTDebug:IDebugger
	{
		public HTDebug ()
		{
		}

		#region IDebugger implementation
		public void Log (string msg)
		{
			Console.WriteLine (msg);
		}
		#endregion
	}
}

