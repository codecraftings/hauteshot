using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;
using System.Collections.Generic;

namespace HauteShot.iOS
{
	partial class UserProfileViewController : BaseUIController
	{
		public new UserProfileViewModel ViewModel {
			get {
				return (UserProfileViewModel)base.ViewModel;
			}
			set {
				base.ViewModel = (BaseViewModel)value;
			}
		}

		public int UserID;

		public UserProfileViewController (IntPtr handle) : base (handle)
		{
			this.ViewModel = new UserProfileViewModel (this);
		}

		public List<AlbumTileView> AlbumViews;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			Footer.Controller = this as BaseUIController;
			OnViewModelUpdated ("all");
			if (UserID != 0) {
				ViewModel.FetchUser (UserID);
			}
			Header.BackButton.TouchUpInside += (object sender, EventArgs e) => {
				if (NavigationController != null) {
					NavigationController.PopViewController (false);
				}
			};
			ScrollContainer.TranslatesAutoresizingMaskIntoConstraints = false;
			Header.TranslatesAutoresizingMaskIntoConstraints = false;
			Footer.TranslatesAutoresizingMaskIntoConstraints = false;
			AlbumTiles.TranslatesAutoresizingMaskIntoConstraints = false;
			this.View.AddConstraint(NSLayoutConstraint.Create(ScrollContainer, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0));
			ScrollContainer.AddConstraint(NSLayoutConstraint.Create(DetailsCard, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ScrollContainer, NSLayoutAttribute.Width, 1, -12));
			ScrollContainer.AddConstraint(NSLayoutConstraint.Create(AlbumTiles, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ScrollContainer, NSLayoutAttribute.Width, 1, -12));
			ScrollContainer.DefineLayout (new string[] {
				"|-6-[DetailsCard]",
				"|-6-[AlbumTiles]",
				"V:|-5-[DetailsCard]-10-[AlbumTiles]-10-|"
			}, "DetailsCard", DetailsCard, "AlbumTiles", AlbumTiles);

			var gesture = new UIHoverAndTapGesture (onBadgesTapped);
			badges.AddGestureRecognizer (gesture);
		}
		public void onBadgesTapped(UIHoverAndTapGesture tap){
			if (tap.State == UIGestureRecognizerState.Began) {
				UIView.Animate (.2, () => {
					badges.BackgroundColor = UIColor.FromRGBA (200, 200, 200, 100);
				});
				return;
			}
			if (tap.State == UIGestureRecognizerState.Cancelled) {
				UIView.Animate (.2, () => {
					badges.BackgroundColor = UIColor.Clear;
				});
				return;
			}
			if (tap.State == UIGestureRecognizerState.Recognized) {
				UIView.Animate (.2, () => {
					badges.BackgroundColor = UIColor.Clear;
				});
				var view = Storyboard.InstantiateViewController ("BadgesView") as BadgesListController;
				view.UserId = ViewModel.UserData.id;
				NavigationController.PushViewController (view, true);
				return;
			}
			return;
		}
		private void updateBadges ()
		{
			if (ViewModel.Badges == null) {
				badge1.Hidden = true;
				badge2.Hidden = true;
				badge3.Hidden = true;
				badge4.Hidden = true;
				ShowUIBusy (badges);
				return;
			}
			List<Badge> b;
			var total = ViewModel.Badges.Count;
			if (total > 4) {
				b = ViewModel.Badges.GetRange (0, 4);
			} else {
				b = ViewModel.Badges;
			}
			HideUIBusy (badges);
			badge1.Hidden = false;
			if (total > 0) {
				badge1.Image = null;
				ShowUIBusy (badge1);
				ImageLoader.Instance.LoadImage (b [0].icon_url, img => {
					badge1.Image = img;
					HideUIBusy (badge1);
				});
			}
			badge2.Hidden = false;
			if (total > 1) {
				badge2.Image = null;
				ShowUIBusy (badge2);
				ImageLoader.Instance.LoadImage (b [1].icon_url, img => {
					badge2.Image = img;
					HideUIBusy (badge2);
				});
			}
			badge3.Hidden = false;
			if (total > 2) {
				badge3.Image = null;
				ShowUIBusy (badge3);
				ImageLoader.Instance.LoadImage (b [2].icon_url, img => {
					badge3.Image = img;
					HideUIBusy (badge3);
				});
			}
			badge4.Hidden = false;
			if (total > 3) {
				badge4.Image = null;
				ShowUIBusy (badge4);
				ImageLoader.Instance.LoadImage (b [3].icon_url, img => {
					badge4.Image = img;
					HideUIBusy (badge4);
				});
			}
		}

		public void updateAlbumTiles ()
		{
			if (ViewModel.Albums == null) {
				ShowUIBusy (AlbumTiles);
				return;
			}
			if (AlbumViews == null) {
				AlbumViews = new List<AlbumTileView> ();
			}
			HideUIBusy (AlbumTiles);


			UIView topView = new UIView();
			AlbumTiles.Add (topView);

			AlbumTiles.DefineLayout (new string[] {
				"|[topView]|",
				"V:|[topView(1)]"
			}, "topView", topView);
			for (var i=0;i < ViewModel.Albums.Count;i++) {
				var album = ViewModel.Albums [i];
				var tile = new AlbumTileView (album.feature_photos.Length);
				AlbumTiles.AddSubview (tile);

				AlbumTiles.DefineLayout (new string[] {
					"|[tile]|",
					"V:[topView]-10-[tile]"
				}, "tile", tile, "topView", topView);
				if (i == (ViewModel.Albums.Count - 1)) {
					//AlbumTiles.ConstrainLayout (() => tile.Frame.Bottom == AlbumTiles.Frame.Bottom);
					AlbumTiles.DefineLayout (new string[]{ "V:[tile]|" }, "tile", tile);
				}
				topView = tile;
				tile.AlbumTitle.Text = album.title;
				tile.onPhotoSelected = onPhotoClicked;
				foreach (var photo in album.feature_photos) {
					ImageLoader.Instance.LoadImage (photo.thumb_url, image => {
						tile.AddPhoto (image, photo.id);
						updateScrollView();
					});
				}
				tile.LayoutSubviews ();
				AlbumViews.Add (tile);
			}
			AlbumTiles.InvalidateIntrinsicContentSize ();
			AlbumTiles.ClipsToBounds = true;
		}
		private void updateScrollView(){
			ScrollContainer.ScrollEnabled = true;
			ScrollContainer.LayoutSubviews ();
			ScrollContainer.ContentSize = new CoreGraphics.CGSize (ScrollContainer.Bounds.Width, AlbumTiles.Frame.Bottom);
		}
		public void onPhotoClicked (int photoId)
		{
			var view = Storyboard.InstantiateViewController ("PhotoDetailViewer") as PhotoViewController;
			view.PhotoID = photoId;
			if (NavigationController != null) {
				NavigationController.PushViewController (view, true);
			}
		}

		public void updateUserCard ()
		{
			if (ViewModel.UserData == null) {
				//DetailsCard.Hidden = true;
				//AlbumTiles.Hidden = true;
				UserPhoto.Image = null;
				this.ShowUIBusy (UserPhoto);
				UserName.Text = "...";
				NickName.Text = "...";
				UserMeta.Text = "...";
				return;
			}
			UserName.Text = ViewModel.UserData.full_name;
			NickName.Text = ViewModel.UserData.username;
			UserMeta.Text = ViewModel.UserData.photos_count + " uploads | " + ViewModel.UserData.promotes_count + " promotes";
			ImageLoader.Instance.LoadImage (ViewModel.UserData.profile_pic_url, img => {
				UserPhoto.Image = img;
				HideUIBusy (UserPhoto);
			});
		}

		public override void OnViewModelUpdated (string updateId)
		{
			base.OnViewModelUpdated (null);

			if (updateId == "all") {
				updateUserCard ();
				updateBadges ();
				updateAlbumTiles ();
			}
			if (updateId == "albumdata") {
				updateAlbumTiles ();
			}
			if (updateId == "badgedata") {
				updateBadges ();
			}
			if (updateId == "userdata") {
				updateUserCard ();
			}
			updateScrollView ();
		}
	}
}
