﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;
using System.Collections.Generic;
using HauteShot.Core;

namespace HauteShot.iOS
{
	[Register ("AlbumTitle"), DesignTimeVisible (false)]
	public class AlbumTileView:UIView, IComponent
	{
		public UILabel AlbumTitle;
		private UIView Separator;
		public List<UIImageView> PhotoTiles;
		private UIView tilesContainer;
		public Action<int> onPhotoSelected;
		private int photoCount;

		public event EventHandler Disposed;

		public ISite Site {
			get;
			set;
		}

		public AlbumTileView (IntPtr p) : base (p)
		{
		}

		public AlbumTileView (int photoCount) : base ()
		{
			this.photoCount = photoCount;
			Initialize ();
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			Initialize ();
		}

		private void Initialize ()
		{
			AlbumTitle = new UILabel {
				Text = "Album title goes here",
				Font = UIFont.SystemFontOfSize (14),
				TextColor = UIColor.FromRGB (76, 76, 76)
			};
			Separator = new UIView {
				BackgroundColor = UIColor.FromRGB (230, 230, 230)
			};

			BackgroundColor = UIColor.White;
			tilesContainer = new UIView ();
			tilesContainer.ClipsToBounds = true;
			PhotoTiles = new List<UIImageView> ();
			AddSubviews (new UIView[]{ AlbumTitle, Separator, tilesContainer });


			this.DefineLayout (new string[] {
				"|-15-[AlbumTitle]-15-|",
				"V:|-6-[AlbumTitle]-3-[Separator(1)]-9-[tilesContainer]-8-|",
				"|-11-[Separator]-11-|",
				"|-15-[tilesContainer]-15-|"
			}, "AlbumTitle", AlbumTitle, "Separator", Separator, "tilesContainer", tilesContainer);
		}

		public void AddPhoto (UIImage image, int id)
		{
			var tile = new UIImageView {
				Image = image,
				ContentMode = UIViewContentMode.ScaleAspectFill,
				BackgroundColor = UIColor.DarkGray,
				ClipsToBounds = true,
				Tag = id
			};
			UIView prev;
			if (PhotoTiles.Count < 1) {
				prev = new UIView ();
				tilesContainer.AddSubview (prev);
				//tilesContainer.ConstrainLayout (() => prev.Frame.Left == tilesContainer.Frame.Left - 5 && prev.Frame.Top == tilesContainer.Frame.Top && prev.Frame.Width == 1 && prev.Frame.Height == 1);
				tilesContainer.DefineLayout (new string[] {
					"|-(-5)-[prev(1)]",
					"V:|[prev(1)]"
				}, "prev", prev);
			} else {
				prev = PhotoTiles [PhotoTiles.Count - 1] as UIView;
			}
			tilesContainer.AddSubview (tile);

			tilesContainer.DefineLayout (new string[] {
				"[prev]-5-[tile(75)]",
				"V:|[tile(75)]|"
			}, "prev", prev, "tile", tile);
			PhotoTiles.Add (tile);
			return;
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			//base.TouchesEnded (touches, evt);
			UITouch touch = touches.AnyObject as UITouch;
			if (touch.TapCount == 1) {
				foreach (var tile in PhotoTiles) {
					if (tile.Frame.Contains (touch.LocationInView (tilesContainer))) {
						if (onPhotoSelected != null) {
							onPhotoSelected ((int)tile.Tag);
						}
					}
				}
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
		}
	}
}

