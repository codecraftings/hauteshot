using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using System.Drawing;
using HauteShot.Core;

namespace HauteShot.iOS
{
	partial class PhotoViewController : BaseUIController
	{
		public new PhotoViewModel ViewModel{
			get{
				return (PhotoViewModel)base.ViewModel;
			}
			set{
				base.ViewModel = (BaseViewModel)value;
			}
		}
		public int PhotoID;
		public bool BackButtonEnabled;
		public PhotoViewController (IntPtr handle) : base (handle)
		{
			ViewModel = new PhotoViewModel (this);
			BackButtonEnabled = true;
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			Header.BackButton.TouchUpInside += (sender, e) => {
				NavigationController.PopViewController(false);
			};
			Header.BackButton.Hidden = !BackButtonEnabled;
			ScrollContainer.ScrollEnabled = true;

			this.View.TranslatesAutoresizingMaskIntoConstraints = false;
			ScrollContainer.TranslatesAutoresizingMaskIntoConstraints = false;

			this.View.AddConstraint(NSLayoutConstraint.Create(ScrollContainer, NSLayoutAttribute.Width, NSLayoutRelation.Equal, View, NSLayoutAttribute.Width, 1, 0));
			//ScrollInner.AddConstraint(NSLayoutConstraint.Create(UserCard, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ScrollInner, NSLayoutAttribute.Width, 1, 0));
			ScrollContainer.AddConstraint(NSLayoutConstraint.Create(ScrollInner, NSLayoutAttribute.Width, NSLayoutRelation.Equal, ScrollContainer, NSLayoutAttribute.Width, 1, 0));
			this.View.DefineLayout (new string[]{ "|[ScrollContainer]|" }, "ScrollContainer", ScrollContainer);
			ScrollContainer.DefineLayout (new string[]{ "|[ScrollInner]|" }, "ScrollInner", ScrollInner);
			ScrollInner.DefineLayout (new string[] {
				"|-6-[UserCard]-6-|",
				"|-6-[PhotoCard]-6-|",
				"|-6-[CaptionCard]-6-|",
				"|-6-[ShareCard]-6-|"
			}, "UserCard", UserCard, "PhotoCard", PhotoCard, "CaptionCard", CaptionCard, "ShareCard", ShareCard);

			//ScrollContainer.ContentSize = new SizeF (ScrollContainer.Frame.Width, 1000);
			Footer.Controller = this as BaseUIController;
			if (PhotoID != 0) {
				ViewModel.LoadPhoto (PhotoID);
			}
			var gesture = new UITapGestureRecognizer (goToUserProfile);
			gesture.CancelsTouchesInView = false;
			UserCard.AddGestureRecognizer (gesture);
			OnViewModelUpdated (null);
		}
		public override void OnViewModelUpdated (string updateId)
		{
			base.OnViewModelUpdated (updateId);
			if (ViewModel.PhotoData == null) {
				UserName.Text = "....";
				TimeLabel.Text = "...";
				UserPhoto.Image = UIImage.FromBundle ("loading.png");
				CaptionCard.Text = "...";
				UserCard.Hidden = true;
				CaptionCard.Hidden = true;
				ShareCard.Hidden = true;
				return;
			}
			UserCard.Hidden = false;
			CaptionCard.Hidden = false;
			ShareCard.Hidden = false;
			UserName.Text = ViewModel.PhotoData.user.full_name;
			TimeLabel.Text = ViewModel.PhotoData.GetHumanDate();
			if (String.IsNullOrWhiteSpace (ViewModel.PhotoData.caption)) {
				CaptionCard.Text = "<No Caption>";
				CaptionCard.Hidden = false;
			} else {
				CaptionCard.Text = ViewModel.PhotoData.caption;
				CaptionCard.Hidden = false;
			}
			CaptionCard.TextContainerInset = new UIEdgeInsets (10, 10, 10, 10);
			ImageLoader.Instance.LoadImage (ViewModel.PhotoData.user.profile_pic_url, img => {
				UserPhoto.Image = img;
			});
			ImageLoader.Instance.LoadImage (ViewModel.PhotoData.original_url, image => {
				PhotoCard.SetImage(image);
				//PhotoCard.ImageHolder.Image = image;
				//reloadLayout();
			});

		}
		public void goToUserProfile(){
			var view = Storyboard.InstantiateViewController ("UserProfileView") as UserProfileViewController;
			view.UserID = ViewModel.PhotoData.user_id;
			NavigationController.PushViewController (view, true);
		}
	}
}
