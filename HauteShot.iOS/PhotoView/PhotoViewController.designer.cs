// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;

namespace HauteShot.iOS
{
	[Register ("PhotoViewController")]
	partial class PhotoViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView CaptionCard { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HTFooterView Footer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HTHeaderView Header { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		PhotoViewer PhotoCard { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView ScrollContainer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView ScrollInner { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView ShareCard { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel TimeLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CardView UserCard { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel UserName { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CircularImage UserPhoto { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (CaptionCard != null) {
				CaptionCard.Dispose ();
				CaptionCard = null;
			}
			if (Footer != null) {
				Footer.Dispose ();
				Footer = null;
			}
			if (Header != null) {
				Header.Dispose ();
				Header = null;
			}
			if (PhotoCard != null) {
				PhotoCard.Dispose ();
				PhotoCard = null;
			}
			if (ScrollContainer != null) {
				ScrollContainer.Dispose ();
				ScrollContainer = null;
			}
			if (ScrollInner != null) {
				ScrollInner.Dispose ();
				ScrollInner = null;
			}
			if (ShareCard != null) {
				ShareCard.Dispose ();
				ShareCard = null;
			}
			if (TimeLabel != null) {
				TimeLabel.Dispose ();
				TimeLabel = null;
			}
			if (UserCard != null) {
				UserCard.Dispose ();
				UserCard = null;
			}
			if (UserName != null) {
				UserName.Dispose ();
				UserName = null;
			}
			if (UserPhoto != null) {
				UserPhoto.Dispose ();
				UserPhoto = null;
			}
		}
	}
}
