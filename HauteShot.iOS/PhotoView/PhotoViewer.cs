﻿using System;
using Foundation;
using System.ComponentModel;
using UIKit;
using System.Drawing;

namespace HauteShot.iOS
{
	[Register ("PhotoView"), DesignTimeVisible (true)]
	public class PhotoViewer:CardView, IComponent
	{
		public UIView View;
		public UIImageView ImageHolder;
		private UIImage image;
		public NSLayoutConstraint ImageHeightConstraint;
		public event EventHandler Disposed;

		public ISite Site {
			get;
			set;
		}

		[Export ("Image"), Browsable (true)]
		public UIImage Image {
			get {
				return image;
			}
			set {
				image = value;
				SetNeedsDisplay ();
			}
		}

		public new CoreGraphics.CGSize IntrinsicContentSize {
			get {
				//return base.IntrinsicContentSize;
				return new CoreGraphics.CGSize (Bounds.Width, ImageHolder.IntrinsicContentSize.Height / ImageHolder.IntrinsicContentSize.Width * (Bounds.Width - 10) + 10);
			}
		}

		public PhotoViewer (IntPtr p) : base (p)
		{
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			ImageHolder = new UIImageView {
				Image = Image,
				ContentMode = UIViewContentMode.ScaleAspectFit,
				ClipsToBounds = true,
				BackgroundColor = UIColor.Clear
			};
			View = new UIView ();
			this.AddSubview (ImageHolder);

			this.DefineLayout (new string[]{ "|-5-[ImageHolder]-5-|", "V:|-5-[ImageHolder]-5-|" }, "ImageHolder", ImageHolder);
			ImageHeightConstraint = NSLayoutConstraint.Create (ImageHolder, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.Height, 1, 300);
			this.AddConstraint (ImageHeightConstraint);
			//this.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;
		}
		public void SetImage(UIImage img){
			ImageHolder.Image = img;
			var ratio = ImageHolder.IntrinsicContentSize.Height / ImageHolder.IntrinsicContentSize.Width;
			ImageHeightConstraint.Constant = ratio * (Bounds.Width - 10);
			this.UpdateConstraints ();
		}
		public override void LayoutSubviews ()
		{
			var padding = 5;

			base.LayoutSubviews ();
		}
	}
}

