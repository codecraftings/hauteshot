﻿using System;
using UIKit;
using HauteShot.Core;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Foundation;
using System.Security.Cryptography;
using System.Threading.Tasks;


namespace HauteShot.iOS
{
	public class ImageLoader:NSObject
	{
		static readonly object _memory_cache_lock = new object ();
		static readonly object _file_cache_lock = new object ();
		static readonly object _down_track_lock = new object ();
		IWebService webService;
		private Dictionary<string, UIImage> OnMemory;
		private LinkedList<string> CachedImages;
		private static ImageLoader _instance;
		private List<string> ActiveDownload;
		private int memoryCacheLimit = 10;
		private int FileCacheLimit = 50;
		public static ImageLoader Instance {
			get {
				if (_instance == null) {
					_instance = new ImageLoader ();
				}
				return _instance;
			}
		}

		private ImageLoader ()
		{
			webService = ServiceContainer.Resolve<IWebService> ();
			OnMemory = new Dictionary<string, UIImage> ();
			ActiveDownload = new List<string> ();
		}

		public async void LoadImage(string url, Action<UIImage> onload){
			UIImage image = null;
			//Console.WriteLine (url);
			await Task.Run (() => {
				image = FromCache(url);
				//Console.WriteLine(url);
				if(image==null){
					image = FromSever(url);
				}
			});
			onload (image);
			InvokeOnMainThread (() => {
				//onload(image);
			});
		}
		private void SaveToMemory (string url, UIImage image)
		{
			lock (_memory_cache_lock) {
				if (OnMemory.Count > memoryCacheLimit) {
					string ass = "";
					foreach (var img in OnMemory) {
						ass = img.Key;
						break;
					}
					OnMemory.Remove (ass);
				}
				if (!OnMemory.ContainsKey (url)) {
					OnMemory.Add (url, image);
				}
			}
		}

		public UIImage FromMemory (string url)
		{
			lock (_memory_cache_lock) {
				if (OnMemory.ContainsKey (url)) {
					return OnMemory [url];
				}
				return null;
			}
		}

		public UIImage FromCache (string url)
		{
			var memory = this.FromMemory (url);
			if (memory != null) {
				Console.WriteLine ("From Memory " + url);
				return memory;
			}
			var id = this.CalculateMD5Hash (url);
			lock (_file_cache_lock) {
				if (CachedImages == null) {
					loadCacheListFromFile ();
				}
				if (!CachedImages.Contains (id)) {
					return null;
				}
			}
			byte[] buffer = null;
			try {
				buffer = File.ReadAllBytes (this.GetPath (id));
			} catch {
				return null;
			}
			NSData data = NSData.FromArray (buffer);
			var image = UIImage.LoadFromData (data);
			this.SaveToMemory (url, image);
			Console.WriteLine ("From File " + url);
			return image;
		}

		private void SaveToCache (string url, byte[] imagedata)
		{
			var md5 = this.CalculateMD5Hash (url);
			lock (_file_cache_lock) {
				if (CachedImages == null) {
					loadCacheListFromFile ();
				}
				if (CachedImages.Count > FileCacheLimit) {
					File.Delete (GetPath (CachedImages.First.Value));
					CachedImages.RemoveFirst ();
				}
			}
			Stream stream = null;
			try {
				stream = new FileStream (GetPath (md5), FileMode.Create);
				stream.Write (imagedata, 0, imagedata.Length);
			} catch {
				return;
			} finally {
				if (stream != null)
					stream.Close ();
			}
			lock (_file_cache_lock) {
				CachedImages.AddLast (md5);
				saveCacheListToFile ();
			}
		}

		public UIImage FromSever (string url)
		{
			lock (_down_track_lock) {
				if (ActiveDownload.Contains (url)) {
					return null;
				}
				ActiveDownload.Add (url);
			}
			var service = webService.Create (url);
			byte[] buffer = service.ReadImageBytes ();
			var data = NSData.FromArray (buffer);
			var image = UIImage.LoadFromData (data);
			SaveToMemory (url, image);
			SaveToCache (url, buffer);
			lock (_down_track_lock) {
				ActiveDownload.Remove (url);
			}
			return image;
		}

		private void saveCacheListToFile ()
		{
			var str = Parser.ToJSON (CachedImages);
			Stream stream = null;
			try {
				stream = new FileStream (GetPath ("cachefilelist.txt"), FileMode.Create);
				byte[] buffer = Encoding.Default.GetBytes (str);
				stream.Write (buffer, 0, buffer.Length);
			} catch {

			} finally {
				stream.Close ();
			}
		}

		private void loadCacheListFromFile ()
		{
			StreamReader stream = null;
			try {
				stream = new StreamReader (new FileStream (GetPath ("cachefilelist.txt"), FileMode.Open));
				var str = stream.ReadToEnd ();
				CachedImages = Parser.getObject<LinkedList<string>> (str);

			} catch {
				CachedImages = new LinkedList<string> ();
			} finally {
				if (stream != null) {
					stream.Close ();
				}
			}
		}

		public void LoadImageIntoView (UIImageView imageView, string url)
		{
			imageView.Image = null;
			//string newImage = await webService.DownloadImageAsync (url);
			//imageView.Image = UIImage.FromBundle (newImage);
		}

		private string GetPath (string md5)
		{
			return Path.Combine (Path.GetTempPath (), md5);
		}

		private string CalculateMD5Hash (string input)
		{
			// step 1, calculate MD5 hash from input
			MD5 md5 = System.Security.Cryptography.MD5.Create ();
			byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes (input);
			byte[] hash = md5.ComputeHash (inputBytes);

			// step 2, convert byte array to hex string
			StringBuilder sb = new StringBuilder ();
			for (int i = 0; i < hash.Length; i++) {
				sb.Append (hash [i].ToString ("X2"));
			}
			return sb.ToString ();
		}

	}
}

