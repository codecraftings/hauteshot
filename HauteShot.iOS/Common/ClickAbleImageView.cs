﻿using System;
using UIKit;
using Foundation;

namespace HauteShot.iOS
{
	public class ClickAbleImageView:UIImageView
	{
		public Action OnTouchUp;
		public Action<ClickAbleImageView> OnImageClicked;
		public ClickAbleImageView ():base()
		{

		}
		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			Console.WriteLine ("here");
			base.TouchesBegan (touches, evt);
		}
		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			UITouch touch = touches.AnyObject as UITouch;
			if (touch.TapCount == 1) {
				if (OnImageClicked != null) {
					OnImageClicked (this);
				}
			}
		}
		public override void TouchesCancelled (NSSet touches, UIEvent evt)
		{
			base.TouchesCancelled (touches, evt);
		}
	}
}

