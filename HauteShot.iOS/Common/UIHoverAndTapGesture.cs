﻿using System;
using UIKit;
using Foundation;

namespace HauteShot.iOS
{
	public class UIHoverAndTapGesture:UIGestureRecognizer
	{
		public int NumberOfTapsRequired = 1;
		public UIHoverAndTapGesture (Action<UIHoverAndTapGesture> handler):base()
		{
			this.AddTarget (()=>{
				handler(this);
			});
		}
		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			State = UIGestureRecognizerState.Began;
		}
		public override void TouchesMoved (NSSet touches, UIEvent evt)
		{
			base.TouchesMoved (touches, evt);
		}
		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			var touch = touches.AnyObject as UITouch;
			if (touch.TapCount == NumberOfTapsRequired) {
				State = UIGestureRecognizerState.Recognized;
			} else {
				State = UIGestureRecognizerState.Cancelled;
			}
		}
		public override void TouchesCancelled (NSSet touches, UIEvent evt)
		{
			base.TouchesCancelled (touches, evt);
			State = UIGestureRecognizerState.Cancelled;
		}
	}
}

