﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;

namespace HauteShot.iOS
{
	[Register("PromoteButton"), DesignTimeVisible(true)]
	public class PromoteButton:UIView
	{
		public int PromoteCount {
			get;
			set;
		}
		public UIImageView Image;
		public PromoteButton (IntPtr p):base(p)
		{

		}
		public PromoteButton(){
			Initialize ();
		}
		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			Initialize ();
		}
		public void Initialize(){
			Image = new UIImageView {
				Image = UIImage.FromBundle ("trophy.png")
			};
			this.AddSubview (Image);
		}

	}
}

