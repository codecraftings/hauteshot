﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;
using CoreGraphics;
using CoreAnimation;
using System.Drawing;

namespace HauteShot.iOS
{
	[Register("CardView"), DesignTimeVisible(true)]
	public class CardView:UIView
	{
		public CardView (IntPtr p):base(p)
		{
		}
		[Export("IsInteractive"), Browsable(true)]
		public bool IsInteractive{ get; set; }
		private float shadowOpacity;
		private SizeF shadowOffset;
		public CardView(bool is_interactive = false):base()
		{
			IsInteractive = is_interactive;
			Initialize ();
		}
		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			Initialize ();
			//this.AddSubview (ContentView);
		}
		private void Initialize(){
			this.BackgroundColor = UIColor.White;
			shadowOpacity = .2f;
			shadowOffset = new SizeF (0.7f, 0.7f);
		}
		public void OnTap(){
			if (!IsInteractive) {
				return;
			}
			var anim = new CABasicAnimation ();
			anim.To = new NSNumber (shadowOpacity);
			anim.From = new NSNumber(0);
			anim.Duration = .3;
			anim.AutoReverses = false;
			Layer.AddAnimation (anim, "shadowOpacity");
			Layer.ShadowOpacity = shadowOpacity;
		}
		private void setShadow(){
			Layer.ShadowPath = CGPath.FromRect (Bounds);
			Layer.ShadowColor = UIColor.Black.CGColor;
			Layer.ShadowOpacity = shadowOpacity;
			Layer.ShadowOffset = shadowOffset;
		}
		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			onEnter ();
		}
		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			var touch = touches.AnyObject as UITouch;
			if (touch.TapCount == 1) {
				OnTap ();
				return;
			}
			onOut ();
		}
		public override void TouchesCancelled (NSSet touches, UIEvent evt)
		{
			base.TouchesCancelled (touches, evt);
			onOut ();
		}
		private void onEnter(){
			if (!IsInteractive) {
				return;
			}
			var anim = new CABasicAnimation ();
			anim.From = new NSNumber (shadowOpacity);
			anim.To = new NSNumber(0);
			anim.Duration = .1;
			anim.AutoReverses = false;
			Layer.AddAnimation (anim, "shadowOpacity");
			Layer.ShadowOpacity = 0;
		}
		private void onOut(){
			if (!IsInteractive) {
				return;
			}
			CATransaction.Begin ();
			CATransaction.AnimationDuration = .4;
			Layer.ShadowOpacity = shadowOpacity;
			CATransaction.Commit ();
		}
		public override void LayoutSubviews ()
		{
			this.setShadow ();
			base.LayoutSubviews ();
		}
	}
}

