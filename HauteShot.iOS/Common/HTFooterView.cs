﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;
using System.Drawing;
using CoreGraphics;

namespace HauteShot.iOS
{
	[Register("FooterView"), DesignTimeVisible(true)]
	public class HTFooterView:UIView
	{
		public UIButton HomeButton;
		public UIButton SettingsButton;
		public UIButton CameraButton;
		public UIButton RankButton;
		public UIButton InfoButton;
		public BaseUIController Controller;

		public HTFooterView (IntPtr p):base(p)
		{
		}
		public override void AwakeFromNib ()
		{
			//base.AwakeFromNib ();
			this.Initialize ();
		}
		private void Initialize(){
			//this.BackgroundColor = UIColor.Blue;
			HomeButton = new UIButton();
			HomeButton.SetImage(UIImage.FromBundle("home.png"), UIControlState.Normal);
			HomeButton.TouchUpInside += (s, o) => gotoView ("HomeFeedView");
			SettingsButton = new UIButton ();
			SettingsButton.SetImage (UIImage.FromBundle ("cog.png"), UIControlState.Normal);
			CameraButton = new UIButton ();
			CameraButton.SetImage (UIImage.FromBundle ("camera.png"), UIControlState.Normal);
			CameraButton.TouchUpInside += (sender, e) => {
				if(Controller!=null){
					if(Editor.Instance.HasCamera())
						Editor.Instance.CameraPicker(Controller.NavigationController);
					else
						Editor.Instance.PhotoPicker(Controller.NavigationController);
				}
			};
			RankButton = new UIButton ();
			RankButton.SetImage (UIImage.FromBundle ("tropy.png"), UIControlState.Normal);
			RankButton.TouchUpInside += (s, o) => gotoView ("BadgesView");
			InfoButton = new UIButton ();
			InfoButton.SetImage (UIImage.FromBundle ("info.png"), UIControlState.Normal);
			this.AddSubviews (new UIView[]{ HomeButton, SettingsButton, CameraButton, RankButton, InfoButton });
			this.Layer.ShadowPath = CGPath.FromRect (this.Layer.Bounds);
			this.Layer.ShadowColor = UIColor.Black.CGColor;
			this.Layer.ShadowOpacity = .5f;
			this.Layer.ShadowOffset = new SizeF (.7f, .7f);
		}
		private void gotoView(string id){
			if (Controller == null || Controller.NavigationController == null) {
				return;
			}
			var view = this.Controller.Storyboard.InstantiateViewController (id) as BaseUIController;
			if(view!=null)
			Controller.NavigationController.PushViewController (view, true);
		}
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			CameraButton.Frame = new CGRect (this.Center.X-20, -15, 40, 40);
			float sW = 30;
			float sH = 30;
			float space = 25;
			HomeButton.Frame = new CGRect (space, (this.Bounds.Height - sH) / 2, sW, sH);
			SettingsButton.Frame = new CGRect (2*space+sW, (this.Bounds.Height - sH) / 2, sW, sH);
			RankButton.Frame = new CGRect (this.Bounds.Width - 2*space-2*sW, (this.Bounds.Height - sH) / 2, sW, sH);
			InfoButton.Frame = new CGRect (this.Bounds.Width - space-sW, (this.Bounds.Height - sH) / 2, sW, sH);
		}
	}
}

