﻿using System;
using UIKit;
using Foundation;
using System.ComponentModel;
using System.Drawing;
using CoreGraphics;

namespace HauteShot.iOS
{
	[Register("HeaderView"), DesignTimeVisible(true)]
	public class HTHeaderView:UIView
	{
		UIImageView Logo;
		public UIButton BackButton;
		[Export("EnableBackButton"), Browsable(true)]
		public bool BackButtonEnabled{
			get;
			set;
		}
		public HTHeaderView (IntPtr p):base(p)
		{
		}
		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			Initialize ();
		}
		private void Initialize(){
			Logo = new UIImageView{
				Image = UIImage.FromBundle("logo.png"),
				BackgroundColor = UIColor.Clear,
				ContentMode = UIViewContentMode.ScaleAspectFit
			};
			BackButton = new UIButton ();
			BackButton.SetImage (UIImage.FromBundle ("back-btn.png"), UIControlState.Normal);
			if (BackButtonEnabled) {
				AddSubview (BackButton);
			}
			BackgroundColor = UIColor.FromRGB (193, 193, 193);
			AddSubview (Logo);
		}
		private void setShadow(){
			this.Layer.ShadowPath = CGPath.FromRect (new CGRect(0, 1, Bounds.Width, Bounds.Height));
			this.Layer.ShadowColor = UIColor.Black.CGColor;
			this.Layer.ShadowOpacity = .5f;
			this.Layer.ShadowOffset = new SizeF (.4f, .4f);
		}
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			setShadow ();
			var logoW = Bounds.Width / 3;
			var logoH = Bounds.Height-33;
			Logo.Frame = new CGRect ((Bounds.Width - logoW) / 2, 25, logoW, logoH);
			BackButton.Frame = new CGRect (15, (Bounds.Height - 15)/2, 30, 30);
		}
	}
}

