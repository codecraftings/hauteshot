// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;

namespace HauteShot.iOS
{
	[Register ("BadgesListController")]
	partial class BadgesListController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HTFooterView Footer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HTHeaderView Header { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIScrollView ScrollContainer { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (Footer != null) {
				Footer.Dispose ();
				Footer = null;
			}
			if (Header != null) {
				Header.Dispose ();
				Header = null;
			}
			if (ScrollContainer != null) {
				ScrollContainer.Dispose ();
				ScrollContainer = null;
			}
		}
	}
}
