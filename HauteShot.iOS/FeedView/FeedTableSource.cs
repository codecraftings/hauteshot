using System;

using UIKit;

using HauteShot.Core;
using Foundation;
using CoreGraphics;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HauteShot.iOS
{
	public class FeedTableSource:UITableViewSource
	{
		public List<PhotoFeed> FeedItems {
			get;
			set;
		}

		string cellID = "feedViewCell";
		public Action<Photo> OnPhotoSelected;
		public Action<int> OnUserSelected;
		public FeedTableSource (List<PhotoFeed> items)
		{
			FeedItems = items;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return FeedItems.Count;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			FeedTableCell cell = tableView.DequeueReusableCell (cellID) as FeedTableCell;
			if (cell == null) {
				cell = new FeedTableCell (cellID);
			}
			PhotoFeed feed = FeedItems [indexPath.Row];
			cell.UserLabel.Text = feed.user.full_name;
			//cell.PhotoHolder.Image = UIImage.FromBundle (feed.thumb_url);
			int uid = indexPath.Row;
			cell.Tag = uid;
			//Console.WriteLine (feed.thumb_url);
			cell.PhotoHolder.Image = null;
			cell.UserPhoto.Image = null;
			ImageLoader.Instance.LoadImage (feed.thumb_url, img => {
				//Console.WriteLine(cell.Tag);
				if(cell.Tag==uid)
				cell.PhotoHolder.Image = img;
			});
			ImageLoader.Instance.LoadImage (feed.user.profile_pic_url, img => {
				//Console.WriteLine(cell.Tag);
				//Console.WriteLine(String.Format("{0}", tableView.VisibleCells.Length));
				//if(tableView.VisibleCells.Contains<UITableViewCell>(cell as UITableViewCell))
				if(cell.Tag==uid)
				cell.UserPhoto.Image = img;
			});
			cell.PhotoData = feed as Photo;
			cell.TimeLabel.Text = cell.PhotoData.GetHumanDate ();
			cell.OnClickPhoto = (photo) => {
				Console.WriteLine("ok");
				if(OnPhotoSelected!=null){
					OnPhotoSelected(photo);
				}
			};
			cell.OnClickUser = id => {
				if (OnUserSelected != null) {
					OnUserSelected (id);
				}
			};
			//cell.UserPhoto.Image = new UIImage(data);
			//imageLoader.LoadImageIntoView (cell.PhotoHolder, "logo.png");
			//cell.Bounds = new RectangleF (0, 0, tableView.Bounds.Width, 520);
			return cell;
		}
	}
}

