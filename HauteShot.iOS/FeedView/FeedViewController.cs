using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;
using HauteShot.Core;
using CoreGraphics;
using System.Collections.Generic;

namespace HauteShot.iOS
{
	partial class FeedViewController : BaseUIController
	{
		public new FeedViewModel ViewModel{
			get{
				return (FeedViewModel)base.ViewModel;
			}
			set{
				base.ViewModel = value as FeedViewModel;
			}
		}
		public FeedViewController (IntPtr handle) : base (handle)
		{
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.FeedTable.RowHeight = FeedTable.Bounds.Width+60;
			this.ViewModel = new FeedViewModel (this);
			//Console.WriteLine (feeds.Count);
			var feedsource = new FeedTableSource(ViewModel.FeedItems);
			FeedTable.Source = feedsource;
			feedsource.OnPhotoSelected += photo => {
				var photoView = Storyboard.InstantiateViewController("PhotoDetailViewer") as PhotoViewController;
				photoView.PhotoID = photo.id;
				NavigationController.PushViewController(photoView, false);
			};
			feedsource.OnUserSelected = id => {
				var view = Storyboard.InstantiateViewController("UserProfileView") as UserProfileViewController;
				view.UserID = id;
				if(NavigationController!=null){
					NavigationController.PushViewController(view, false);
				}
			};
			FeedTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			this.OnViewModelUpdated (null);
			Footer.Controller = this as BaseUIController;
			Footer.CameraButton.TouchUpInside += (object sender, EventArgs e) => {
				//var picker = new UIImagePickerController ();
				//picker.SourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum;
				//picker.MediaTypes = UIImagePickerController.AvailableMediaTypes (UIImagePickerControllerSourceType.Camera);
				//NavigationController.PresentViewController(Editor.Instance.PhotoPicker(), true, null);
				//Editor.Instance.PhotoPicker(this.NavigationController);
			};
			ViewModel.FetchFeeds ();
		}
		public override void OnViewModelUpdated(string updateId){
			if (ViewModel.LoggedIn == false) {
				var loginView = Storyboard.InstantiateViewController ("LoginView") as LoginViewController;
				NavigationController.PushViewController (loginView, false);
			}
			if (ViewModel.FeedItems.Count < 1) {
				LoadingView.StartAnimating ();
			} else {
				LoadingView.StopAnimating ();
			}
			(FeedTable.Source as FeedTableSource).FeedItems = ViewModel.FeedItems;
			this.FeedTable.ReloadData ();
		}
	}
}
