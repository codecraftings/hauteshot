// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using System;
using Foundation;
using UIKit;
using System.CodeDom.Compiler;

namespace HauteShot.iOS
{
	[Register ("FeedViewController")]
	partial class FeedViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView FeedTable { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HTFooterView Footer { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		HTHeaderView Header { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIActivityIndicatorView LoadingView { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (FeedTable != null) {
				FeedTable.Dispose ();
				FeedTable = null;
			}
			if (Footer != null) {
				Footer.Dispose ();
				Footer = null;
			}
			if (Header != null) {
				Header.Dispose ();
				Header = null;
			}
			if (LoadingView != null) {
				LoadingView.Dispose ();
				LoadingView = null;
			}
		}
	}
}
