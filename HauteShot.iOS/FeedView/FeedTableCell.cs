﻿using System;
using UIKit;
using System.ComponentModel;
using Foundation;
using System.Drawing;
using CoreGraphics;
using HauteShot.Core;

namespace HauteShot.iOS
{
	[Register ("FeedTableCell"), DesignTimeVisible (true)]
	public class FeedTableCell:UITableViewCell
	{
		public UILabel UserLabel;
		public UILabel TimeLabel;
		public UIButton PromoteButton;
		public UILabel PromoteCount;
		public UIImageView PhotoHolder;
		public UIImageView UserPhoto;

		public Photo PhotoData {
			get;
			set;
		}

		public Action<Photo> OnClickPhoto;
		public Action<int> OnClickUser;

		public FeedTableCell (IntPtr p) : base (p)
		{

		}

		public FeedTableCell (string cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			this.Initialize ();
		}

		public override void AwakeFromNib ()
		{
			//base.AwakeFromNib ();
			this.Initialize ();
		}

		private void Initialize ()
		{
			UserLabel = new UILabel {
				Text = "Image Label Here",
				Font = UIFont.SystemFontOfSize (14),
				TextColor = UIColor.DarkGray,
			};
			TimeLabel = new UILabel {
				Text = "....",
				Font = UIFont.SystemFontOfSize (12),
				TextColor = UIColor.Gray
			};
			PromoteCount = new UILabel {
				Text = "00",
				Font = UIFont.SystemFontOfSize (11),
				TextColor = UIColor.FromRGB (224, 221, 38),
				TextAlignment = UITextAlignment.Center
			};
			PromoteCount.Layer.BorderWidth = 1f;
			PromoteCount.Layer.BorderColor = UIColor.FromRGB (224, 221, 38).CGColor;
			PromoteButton = new UIButton ();
			PromoteButton.SetImage (UIImage.FromBundle ("tropy.png"), UIControlState.Normal);

			PromoteButton.TouchDown += (object sender, EventArgs e) => {
				PromoteButton.BackgroundColor = UIColor.FromRGB (211, 211, 211);
			};
			PromoteButton.TouchCancel += (object sender, EventArgs e) => PromoteButton.BackgroundColor = UIColor.Clear;
			PromoteCount.Layer.CornerRadius = 5;
			UserPhoto = new UIImageView {
				BackgroundColor = UIColor.FromRGB (43, 43, 43),
				ContentMode = UIViewContentMode.ScaleAspectFit,
				ClipsToBounds = true
			};
			UserPhoto.Layer.CornerRadius = 20;
			PhotoHolder = new UIImageView {
				BackgroundColor = UIColor.Gray,
				ContentMode = UIViewContentMode.ScaleAspectFit
			};
			this.BackgroundColor = UIColor.Clear;
			this.ContentView.BackgroundColor = UIColor.White;

			ContentView.AddSubviews (new UIView[]{ UserLabel, TimeLabel, PromoteButton, PromoteCount, UserPhoto, PhotoHolder });
			//ContentView.BackgroundColor = UIColor.Cyan;

		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			UITouch touch = touches.AnyObject as UITouch;
			//Console.WriteLine (touch.TapCount);
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			UITouch touch = touches.AnyObject as UITouch;
			if (touch != null && touch.TapCount == 1) {
				if (PhotoHolder.Frame.Contains (touch.LocationInView (this))) {
					Console.WriteLine ("ImageTapped");
					SetShadow (.3f);
					if (OnClickPhoto != null) {
						OnClickPhoto (PhotoData);
					}
				}
				if(UserPhoto.Frame.Contains(touch.LocationInView(this))||UserLabel.Frame.Contains(touch.LocationInView(this))){
					if(OnClickUser!=null){
						OnClickUser(PhotoData.user_id);
					}
				}
			}
		}

		private void SetShadow (float size = .5f, float opacity = .3f)
		{
			this.ContentView.Layer.ShadowColor = UIColor.Black.CGColor;
			this.ContentView.Layer.ShadowOpacity = opacity;
			this.ContentView.Layer.ShadowOffset = new SizeF (size, size);
			this.ContentView.Layer.ShadowPath = CGPath.FromRect (ContentView.Bounds);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			ContentView.Frame = new CGRect (4, 0, Bounds.Width - 8, Bounds.Height - 20);
			this.SetShadow ();
			UserPhoto.Frame = new CGRect (7, 5, 40, 40);
			UserLabel.Frame = new CGRect (57, 7, ContentView.Bounds.Width - 30, 20f);
			TimeLabel.Frame = new CGRect (57, 22, ContentView.Bounds.Width - 30, 20f);
			PromoteButton.Frame = new CGRect (ContentView.Bounds.Width - 50, 0, 50, 50);
			PromoteButton.Layer.Bounds = new CGRect (5, 5, PromoteButton.Frame.Width - 10, PromoteButton.Frame.Height - 10);
			PromoteCount.Frame = new CGRect (ContentView.Bounds.Width - 83, 14, 30, 20);
			PhotoHolder.Frame = new CGRect (2f, 50f, ContentView.Bounds.Width - 4, ContentView.Bounds.Height - 52);
		}
	}
}

