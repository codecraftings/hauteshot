﻿using System;
using Newtonsoft.Json;
using Humanizer;
namespace HauteShot.Core
{
	public class Badge
	{
		public string title;
		public string caption;
		public Photo winner;
		public User user;
		public DateTime contest_start;
		public DateTime contest_end;
		public string contest_status;
		public string content_type;
		public string icon_url;

		public Badge ()
		{
		}
		public string GetEndDateInDays(){
			return contest_end.Humanize ();
		}
	}
}

