﻿using System;
using Newtonsoft.Json;
using Humanizer;
namespace HauteShot.Core
{
	public class Photo
	{
		public int id;

		public int user_id;
		public string original_url;
		public string thumb_url;
		public string caption;
		public int promotes_count;
		public int view_count;
		public int points;
		public string type;
		public DateTime created_at;
		public DateTime updated_at;

		public Badge[] badges;

		public User user;
		public Photo ()
		{
		}
		public string GetHumanDate(){
			return created_at.Humanize ();
		}
	}
}

