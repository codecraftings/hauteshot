﻿using System;

namespace HauteShot.Core
{
	public class User
	{
		public int id {
			set;
			get;
		}

		public string full_name {
			set;
			get;
		}

		public string email {
			get;
			set;
		}

		public string username {
			get;
			set;
		}

		public string profile_pic_url {
			get;
			set;
		}
		public int photos_count;
		public int promotes_count;
		public Album[] albums{ get; set;}

		public Badge[] badges{ get; set; }
		public User ()
		{
		}
	}
}

