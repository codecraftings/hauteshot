﻿using System;

namespace HauteShot.Core
{
	public class Error
	{
		public int Code;
		public string Message;

		public Error (int code, string msg)
		{
			this.Code = code;
			this.Message = msg;
		}
	}
}

