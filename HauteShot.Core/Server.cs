﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace HauteShot.Core
{
	public class Server
	{
		public const string API_BASE = "http://smarttechie.me/webapps/dev/hauteshot-web/public/api/v1/";
		public const string LOGIN_URL = "http://smarttechie.me/webapps/dev/hauteshot-web/public/login";
		public const string SIGNUP_URL = "http://smarttechie.me/webapps/dev/hauteshot-web/public/register";
		private IWebService webService;
		private ISettings userSettings;
		public Error LastError;

		public string Access_Token {
			get {
				if (userSettings != null) {
					return userSettings.UserToken;
				} else {
					return null;
				}
			}
		}

		private static Server _instance;

		public static Server Instance {
			get {
				if (_instance == null) {
					_instance = new Server ();
				}
				return _instance;
			}
		}

		private Server ()
		{
			userSettings = ServiceContainer.Resolve<ISettings> ();
			webService = ServiceContainer.Resolve<IWebService> ();
		}

		public bool Login (string username, string password)
		{
			var service = webService.Create (Server.LOGIN_URL);
			service.SetGetParam ("app_id", App.Instance.ID.ToString ());
			service.SetPostParam ("username", username);
			service.SetPostParam ("password", password);
			if (!service.SendPost ()) {
				handleError(100, "Error connecting to the server");
				return false;
			}
			var response = service.ParseResponse<ApiResponse<TokenResponse>> ();
			if (response.status != 200) {
				handleError(response.status, response.error.message);
				return false;
			}
			//var tokendata = response.GetData<TokenResponse> ();
			userSettings.UserToken = response.data.token;
			userSettings.CurrentUserID = response.data.user_id;
			userSettings.save ();
			return true;
		}

		public bool Register (string name, string email, string username, string password)
		{
			var service = webService.Create (Server.SIGNUP_URL);
			service.SetGetParam ("app_id", App.Instance.ID.ToString ());
			service.SetPostParam ("username", username);
			service.SetPostParam ("password", password);
			service.SetPostParam ("full_name", name);
			service.SetPostParam ("email", email);
			if (!service.SendPost ()) {
				handleError(100, "Error connecting to the server");
				return false;
			}
			var response = service.ParseResponse<ApiResponse<TokenResponse>> ();
			if (response.status != 200) {
				handleError(response.status, response.error.message);
				return false;
			}
			//var tokendata = response.GetData<TokenResponse> ();
			userSettings.UserToken = response.data.token;
			userSettings.CurrentUserID = response.data.user_id;
			userSettings.save ();
			return true;
		}
		public Photo UploadPhoto(string path, Action<float> onProgress){
			var service = webService.Create (Server.API_BASE + "photos");
			InitService (service);
			service.SetPostParam ("user_id", userSettings.CurrentUserID.ToString());
			service.SetPostParam ("caption", "");
			service.SetFileParam ("photo", path);
			service.OnProgress += onProgress;
			if (!service.SendPost ()) {
				handleError(100, "Error connecting to the server");
				return null;
			}
			var response = service.ParseResponse<ApiResponse<Photo>> ();
			if (response.status != 200) {
				handleError(response.status, response.error.message);
				return null;
			}
			return response.data;
		}
		private void InitService(IWebService service){
			service.SetGetParam ("app_id", App.Instance.ID.ToString ());
			service.SetGetParam ("access_token", Access_Token);
		}
		public List<PhotoFeed> GetHomeFeed (int offset = 0)
		{
			var service = webService.Create (Server.API_BASE + "feeds");
			InitService (service);
			if (!service.SendGet ()) {
				handleError(100, "Error connecting to the server");
				return null;
			}
			var response = service.ParseResponse<ApiResponse<List<PhotoFeed>>> ();
			if (response.status != 200) {
				handleError(response.status, response.error.message);
				return null;
			}
			return response.data;
		}

		public User GetUser (int uid)
		{
			var service = webService.Create (String.Format ("{0}users/{1}", Server.API_BASE, uid));
			InitService (service);
			if (!service.SendGet ()) {
				handleError(100, "Error connecting to the server");
				return null;
			}
			var response = service.ParseResponse<ApiResponse<User>> ();
			if (response.status != 200) {
				handleError(response.status, response.error.message);
				return null;
			}
			return response.data;
		}

		public Photo GetPhoto (int pid)
		{
			var service = webService.Create (String.Format ("{0}photos/{1}", Server.API_BASE, pid));
			InitService (service);
			if (!service.SendGet ()) {
				handleError(100, "Error connecting to the server");
				return null;
			}
			var response = service.ParseResponse<ApiResponse<Photo>> ();
			if (response.status != 200) {
				handleError(response.status, response.error.message);
				return null;
			}
			return response.data;
		}

		public List<Album> GetUserAlbums (int uid)
		{
			var service = webService.Create (String.Format ("{0}users/{1}/albums", Server.API_BASE, uid));
			InitService (service);
			if (!service.SendGet ()) {
				return null;
			}
			var response = service.ParseResponse<ApiResponse<List<Album>>> ();
			if (response.status != 200) {
				handleError(response.status, response.error.message);
				return null;
			}
			return response.data;
		}

		public List<PhotoFeed> GetAlbumPhotos (Album album)
		{
			var service = webService.Create (String.Format ("{0}users/{1}/feeds", Server.API_BASE, album.user_id));
			InitService (service);
			service.SetGetParam ("date1", album.date_range [0].ToString());
			service.SetGetParam ("date2", album.date_range [1].ToString());
			if (!service.SendGet ()) {
				handleError(100, "Error connecting to the server");
				return null;
			}
			var response = service.ParseResponse<ApiResponse<List<PhotoFeed>>> ();
			if (response.status != 200) {
				handleError(response.status, response.error.message);
				return null;
			}
			return response.data;
		}

		public List<Badge> GetUserBadges (int uid)
		{
			var service = webService.Create (String.Format ("{0}users/{1}/badges", Server.API_BASE, uid));
			InitService (service);
			if (!service.SendGet ()) {
				handleError(100, "Error connecting to the server");
				return null;
			}
			var response = service.ParseResponse<ApiResponse<List<Badge>>> ();
			if (response.status != 200) {
				handleError(response.status, response.error.message);
				return null;
			}
			return response.data;
		}
		public List<Badge> GetLatestBadges ()
		{
			var service = webService.Create (String.Format ("{0}badges", Server.API_BASE));
			InitService (service);
			if (!service.SendGet ()) {
				handleError(100, "Error connecting to the server");
				return null;
			}
			var response = service.ParseResponse<ApiResponse<List<Badge>>> ();
			if (response.status != 200) {
				handleError(response.status, response.error.message);
				return null;
			}
			return response.data;
		}

		public List<Badge> GetActiveBadges ()
		{
			var service = webService.Create (String.Format ("{0}badges", Server.API_BASE));
			InitService (service);
			service.SetGetParam ("active", "1");
			if (!service.SendGet ()) {
				handleError(100, "Error connecting to the server");
				return null;
			}
			var response = service.ParseResponse<ApiResponse<List<Badge>>> ();
			if (response.status != 200) {
				handleError(response.status, response.error.message);
				return null;
			}
			return response.data;
		}

		public Badge GetBadge (int id)
		{
			var service = webService.Create (String.Format ("{0}badges/{1}", Server.API_BASE, id));
			InitService (service);
			if (!service.SendGet ()) {
				handleError(100, "Error connecting to the server");
				return null;
			}
			var response = service.ParseResponse<ApiResponse<Badge>> ();
			if (response.status != 200) {
				handleError(response.status, response.error.message);
				return null;
			}
			return response.data;
		}
		private void handleError(int code, string msg){
			this.LastError = new Error (code, msg);
		}
		private class TokenResponse
		{
			public string token;
			public string app_id;
			public int user_id;
			public string expire_at;
		}
	}
}

