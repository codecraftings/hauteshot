﻿using System;

namespace HauteShot.Core
{
	public class LoginViewModel:BaseViewModel
	{
		public string UserName;
		public string Password;
		public string Error;
		public LoginViewModel ()
		{

		}
		public bool TryLogin(){
			if (Server.Instance.Login(this.UserName, this.Password)) {
				return true;
			}
			this.Error = Server.Instance.LastError.Message;
			return false;
		}
	}
}

