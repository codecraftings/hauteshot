﻿using System;
using System.Threading.Tasks;

namespace HauteShot.Core
{
	public class SignupViewModel:BaseViewModel
	{
		public string Name;
		public string Email;
		public string Username;
		public string Password;
		public Error Error;
		public SignupViewModel ()
		{
		}
		public async Task<bool> TrySignup(){
			var t = await Task<bool>.Run (() => {
				return Server.Instance.Register(Name, Email, Username, Password);
			});
			if (!t) {
				Error = Server.Instance.LastError;
			}
			return t;
		}
	}
}

