﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class UserProfileViewModel:BaseViewModel
	{
		public User UserData;
		public List<Album> Albums;
		public List<Badge> Badges;
		private IUserRepo userRepo;
		public UserProfileViewModel (IViewController ui)
		{
			UIController = ui;
			userRepo = ServiceContainer.Resolve<IUserRepo> ();
		}
		public void FetchUser(int id){
			userRepo.GetAsync (id, (User obj) => {
				UserData = obj;
				UIController.NotifyViewModelUpdated("userdata");
			});
			userRepo.GetAlbumsAsync (id, (List<Album> obj) => {
				Albums = obj;
				UIController.NotifyViewModelUpdated ("albumdata");
			});
			userRepo.GetBadgesAsync (id, (List<Badge> obj) => {
				Badges = obj;
				UIController.NotifyViewModelUpdated ("badgedata");
			});
		}
	}
}

