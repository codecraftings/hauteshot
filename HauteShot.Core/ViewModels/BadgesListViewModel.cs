﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class BadgesListViewModel:BaseViewModel
	{
		public List<Badge> BadgesList;
		private IBadgeRepo badgeRepo;

		public BadgesListViewModel (IViewController ui)
		{
			this.UIController = ui;
			badgeRepo = ServiceContainer.Resolve<IBadgeRepo> ();
		}

		public void FetchUsersBadges (int userId)
		{
			badgeRepo.getUserBadgesAsync (userId, badges => {
				BadgesList = badges;
				UIController.NotifyViewModelUpdated ("userBadges");
			});
		}

		public void FetchLatestBadges ()
		{
			badgeRepo.getLatestBadgesAsync (badges => {
				BadgesList = badges;
				UIController.NotifyViewModelUpdated ("latesBadges");
			});
		}
	}
}

