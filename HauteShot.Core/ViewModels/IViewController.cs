﻿using System;

namespace HauteShot.Core
{
	public interface IViewController:IDisposable
	{
		BaseViewModel ViewModel {
			get;
			set;
		}
		void CallDataChangeAction(Action action);
		bool IsVisible();
		void OnViewModelUpdated(string updateId);
		void NotifyViewModelUpdated();
		void NotifyViewModelUpdated(string updateId);
	}
}

