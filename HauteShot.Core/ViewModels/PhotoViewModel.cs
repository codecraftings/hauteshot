﻿using System;

namespace HauteShot.Core
{
	public class PhotoViewModel:BaseViewModel
	{
		public Photo PhotoData {
			get;
			set;
		}
		private IPhotoRepo photoRepo;
		public PhotoViewModel (IViewController ui)
		{
			this.UIController = ui;
			photoRepo = ServiceContainer.Resolve<IPhotoRepo> ();
		}
		public void LoadPhoto(int id){
			photoRepo.GetAsync (id, photo => {
				PhotoData = photo;
				UIController.NotifyViewModelUpdated();
			});
		}
	}
}

