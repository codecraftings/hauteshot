﻿using System;
using System.Threading.Tasks;

namespace HauteShot.Core
{
	public class EditorViewModel:BaseViewModel
	{
		public EditorViewModel (IViewController UI):base()
		{
			UIController = UI;
		}
		public async void UploadImage(string path, Action<float> onProgress, Action<Photo> onDone){
			await Task.Run (() => {
				var photo = Server.Instance.UploadPhoto(path, onProgress);
				onProgress(1f);
				onDone(photo);
			});
		}
	}
}

