﻿using System;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public class FeedViewModel:BaseViewModel
	{
		public List<PhotoFeed> FeedItems;
		private IPhotoRepo photoRepo;
		public bool LoggedIn;
		public FeedViewModel (IViewController uicontrol)
		{
			UIController = uicontrol;
			FeedItems = new List<PhotoFeed> ();
			photoRepo = ServiceContainer.Resolve<IPhotoRepo> ();
			LoggedIn = true;
		}
		public void FetchFeeds(){
			photoRepo.GetPhotoFeedAsync (feeds=>{
				if(feeds==null&&photoRepo.LastError.Code==401){
					LoggedIn = false;
				}
				FeedItems = feeds;
				UIController.OnViewModelUpdated(null);
			});
		}
	}
}

