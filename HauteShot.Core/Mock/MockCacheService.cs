﻿using System;

namespace HauteShot.Core
{
	public class MockCacheService:ICacheService
	{
		public MockCacheService ()
		{
		}
		public bool Has(string resourceId){
			return false;
		}
		public string Get(string resourceId){
			return null;
		}
		public void Save(string resourceId, string data){

		}
		public void Clear(){

		}
	}
}

