﻿using System;

namespace HauteShot.Core
{
	public class App
	{
		private static App _instance;
		public static App Instance{
			get{
				if (_instance == null) {
					_instance = new App ();
				}
				return _instance;
			}
		}
		private int _id;
		public int ID{
			get{
				return _id;
			}
		}
		public static IDebugger Debugger{
			get{
				return ServiceContainer.Resolve<IDebugger> ();
			}
		}
		private App(){
			ServiceContainer.Register<IUserRepo> (() => new UserRepo ());
			ServiceContainer.Register<IPhotoRepo> (() => new PhotoRepo ());
			ServiceContainer.Register<IBadgeRepo> (() => new BadgeRepo ());
			ServiceContainer.Register<ICacheService> (() => new MockCacheService ());
			//ServiceContainer.Register<IWebService>(()=> new MockWebService());
		}
		public void OnSetupDroid(){

		}
		public void OnSetupIOS(){
			this._id = 1234;
		}
		public void OnSetupTest(){
			this._id = 1234;
		}
	}
}

