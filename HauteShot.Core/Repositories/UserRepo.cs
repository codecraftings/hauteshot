﻿using System;

using Newtonsoft.Json;
using System.Threading.Tasks;

namespace HauteShot.Core
{
	public class UserRepo:IUserRepo
	{
		private readonly IWebService webservice;
		private readonly ICacheService cacheservice;
		private readonly ISettings settings;

		public UserRepo ()
		{
			this.webservice = ServiceContainer.Resolve<IWebService> ();
			this.cacheservice = ServiceContainer.Resolve<ICacheService> ();
			this.settings = ServiceContainer.Resolve<ISettings> ();

		}

		public async Task GetAsync (int id, Action<User> onDone)
		{
			await Task.Run(()=>{
				var user = Server.Instance.GetUser(id);
				onDone(user);
			});
		}

		public User Get(int id, bool cache = true){
			string str;
			if (cache && this.cacheservice.Has ("user/" + id)) {
				str = this.cacheservice.Get ("user/" + id);
			} else {
				str = "";
				//str = this.webservice.CallApi ("user/" + id, null, "get");
				this.cacheservice.Save ("user/" + id, str);
			}
			User user = JsonConvert.DeserializeObject<User> (str);
			return user;
		}

		public async Task GetAlbumsAsync (int id, Action<System.Collections.Generic.List<Album>> onDone)
		{
			await Task.Run(()=>{
				var albums = Server.Instance.GetUserAlbums(id);
				onDone(albums);
			});
		}


		public async Task GetBadgesAsync (int id, Action<System.Collections.Generic.List<Badge>> onDone)
		{
			await Task.Run(()=>{
				var badges = Server.Instance.GetUserBadges(id);
				onDone(badges);
			});
		}

		public bool IsLoggedIn(){
			return this.settings.UserToken != null&&this.settings.CurrentUserID!=0;
		}

		public User GetCurrentUser(){
			var id = this.settings.CurrentUserID;
			if (this.settings.UserToken == null||id==0) {
				return null;
			}
			return this.Get (id, true);
		}

		public void SetCurrentUser(int id){
			this.settings.UserToken = "TestToken";
			this.settings.CurrentUserID = id;
		}
	}
}

