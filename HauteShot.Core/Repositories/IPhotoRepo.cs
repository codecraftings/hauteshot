﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public interface IPhotoRepo
	{
		Error LastError{ get; set; }
		Task GetPhotoFeedAsync(Action<List<PhotoFeed>> onDone);
		Task GetAsync(int id, Action<Photo> onDone);
	}
}

