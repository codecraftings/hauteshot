﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace HauteShot.Core
{
	public class PhotoRepo:IPhotoRepo
	{
		private readonly IWebService webservice;
		private readonly ICacheService cacheservice;
		private readonly IUserRepo userRepo;
		public Error LastError{ get; set; }
		public PhotoRepo ()
		{
			this.webservice = ServiceContainer.Resolve<IWebService> ();
			this.cacheservice = ServiceContainer.Resolve<ICacheService> ();
			this.userRepo = ServiceContainer.Resolve<IUserRepo> ();
		}
		public async Task GetAsync(int id, Action<Photo> onDone){
			await Task.Run (() => {
				var photo = Server.Instance.GetPhoto(id);
				if(photo!=null){
					onDone(photo);
				}
			});
		}
		public List<Photo> GetAll(bool cache = true){
			string str;
			if (cache && this.cacheservice.Has ("photos/all")) {
				str = this.cacheservice.Get ("photos/all");
			} else {
				str = "";
				//str = this.webservice.CallApi ("photos", null, "get");
			}
			return JsonConvert.DeserializeObject<List<Photo>> (str);
		}
		public List<PhotoFeed> GetPhotoFeed(){
			var feeds = Server.Instance.GetHomeFeed ();
			return feeds;
		}
		public async Task GetPhotoFeedAsync(Action<List<PhotoFeed>> onDone){
			var feeds = await Task.Run<List<PhotoFeed>> (() => {
				return Server.Instance.GetHomeFeed ();
			});
			if (feeds == null) {
				this.LastError = Server.Instance.LastError;
			}
			onDone (feeds);
		}
		private void handleError(int code, string msg){
			LastError = new Error (code, msg);
		}
	}
}

