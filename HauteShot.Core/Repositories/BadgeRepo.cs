﻿using System;
using System.Threading.Tasks;

namespace HauteShot.Core
{
	public class BadgeRepo:IBadgeRepo
	{
		public BadgeRepo ()
		{
		}

		public void getUserBadgesAsync (int user_id, Action<System.Collections.Generic.List<Badge>> onDone)
		{
			Task.Run (() => {
				var badges = Server.Instance.GetUserBadges(user_id);
				onDone(badges);
			});
		}

		public void getLatestBadgesAsync (Action<System.Collections.Generic.List<Badge>> onDone)
		{
			Task.Run (() => {
				var badges = Server.Instance.GetLatestBadges();
				onDone(badges);
			});
		}
	}
}

