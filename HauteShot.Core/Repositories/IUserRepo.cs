﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public interface IUserRepo
	{
		Task GetAsync (int id, Action<User> onDone);
		User Get(int id, bool cache);
		Task GetAlbumsAsync(int id, Action<List<Album>> onDone);
		Task GetBadgesAsync (int id, Action<List<Badge>> onDone);
		User GetCurrentUser();
		void SetCurrentUser(int id);
		bool IsLoggedIn();
	}
}

