﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public interface IBadgeRepo
	{
		void getUserBadgesAsync(int user_id, Action<List<Badge>> onDone);
		void getLatestBadgesAsync(Action<List<Badge>> onDone);
	}
}

