﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HauteShot.Core
{
	public class WebServiceFuck
	{
		private string URL;
		public Dictionary<string, string> GetParams;
		public Dictionary<string, string> PostParams;
		public Dictionary<string, string> FileParams;
		public string Response;
		public Action<string> OnDataReceived;
		public Action<int, int> OnDataSendProgress;
		public Action<int, int> OnDataReceiveProgress;
		private readonly string contentBoundary = "-----A34B324s";

		public WebServiceFuck (string url)
		{
			URL = url;
			GetParams = new Dictionary<string, string> ();
			PostParams = new Dictionary<string, string> ();
			FileParams = new Dictionary<string, string> ();
		}

		public async Task<string> SendGet ()
		{
			var request = HttpWebRequest.Create (this.getUrl());
			request.Method = "GET";
			HttpWebResponse response;
			try{
			response = await Task<WebResponse>.Factory.FromAsync (request.BeginGetResponse, request.EndGetResponse, request) as HttpWebResponse;
			}
			catch(WebException e){
				if (e.Response == null) {
					return null;
				}
				response = e.Response as HttpWebResponse;
			}
			if (response.StatusCode != HttpStatusCode.OK) {

			}
			return readResponse (response);
		}
		public async Task<string> SendPost ()
		{
			HttpWebRequest request = HttpWebRequest.Create (this.getUrl()) as HttpWebRequest;
			request.Method = "POST";
			request.ContentType = String.Format ("multipart/form-data; boundary={0}", this.contentBoundary);
			var posData = this.preparePostData ();
			using(var stream = await Task<Stream>.Factory.FromAsync(request.BeginGetRequestStream, request.EndGetRequestStream, request)){
				byte[] buffer = Encoding.UTF8.GetBytes (posData);
				stream.Write (buffer, 0, buffer.Length);
			}
			HttpWebResponse response;
			try{
				response = await Task<WebResponse>.Factory.FromAsync (request.BeginGetResponse, request.EndGetResponse, request) as HttpWebResponse;
			}
			catch(WebException e){
				if (e.Response == null) {
					return null;
				}
				response = e.Response as HttpWebResponse;
			}
			if (response.StatusCode != HttpStatusCode.OK) {

			}
			return readResponse (response);
		}

		public void SendDelete ()
		{

		}

		public void SendPut ()
		{

		}

		public void Dispose ()
		{

		}
		private string getUrl(){
			var query = this.prepareQueryParamsString ();
			return String.Format ("{0}?{1}", URL, query);
		}
		private string prepareQueryParamsString ()
		{
			var str = new StringBuilder ();
			int i = 0;
			foreach (var param in GetParams) {
				if (i == (GetParams.Count - 1)) {
					str.AppendFormat ("{0}={1}", WebUtility.UrlEncode (param.Key), WebUtility.UrlEncode (param.Value));
				} else {
					str.AppendFormat ("{0}={1}&", WebUtility.UrlEncode (param.Key), WebUtility.UrlEncode (param.Value));
				}
				i += 1;
			}
			return str.ToString ();
		}
		private string preparePostData(){
			var str = new StringBuilder ();
			var boundary = String.Format ("--{0}", this.contentBoundary);
			foreach (var param in PostParams) {
				str.AppendLine (boundary);
				str.AppendLine (String.Format ("Content-Disposition: form-data; name=\"{0}\"", param.Key));
				str.AppendLine ();
				str.AppendLine (param.Value);
			}
			foreach (var param in FileParams) {
				str.AppendLine (boundary);
				str.AppendLine (String.Format ("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}.png\"", param.Key, "mbdd"));
				str.AppendLine ("Content-Type: image/png");
				str.AppendLine();
				str.AppendLine (param.Value);
			}
			str.AppendLine (String.Format("{0}--", boundary));
			return str.ToString ();
		}
		private string readResponse(HttpWebResponse response){
			using (var responseStream = response.GetResponseStream ()) {
				var reader = new StreamReader (responseStream);
				var content = reader.ReadToEnd ();
				response.Dispose ();
				return content;
			}
		}
	}
}