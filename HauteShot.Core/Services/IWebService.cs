﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace HauteShot.Core
{
	public interface IWebService:IDisposable
	{
		Action<float> OnProgress{ get; set; }
		IWebService Create(string url);
		bool SendGet();
		bool SendPost();
		bool SendDelete();
		bool SendPut();
		void HandleOnProgress (float progress);
		string ReadResponseAsString ();
		T ParseResponse<T>();
		byte[] ReadImageBytes ();
		void SetGetParam(string key, string value);
		void SetPostParam(string key, string value);
		void SetFileParam(string key, string filepath);
	}
}

