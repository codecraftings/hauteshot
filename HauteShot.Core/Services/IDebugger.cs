﻿using System;

namespace HauteShot.Core
{
	public interface IDebugger
	{
		void Log(string msg);
	}
}

