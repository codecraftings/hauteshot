﻿using System;

namespace HauteShot.Core
{
	public class ApiResponse<T>
	{
		public int status;
		public T data;
		public ApiError error;
		public ApiResponse ()
		{

		}
		public T GetData<T>(){
			var str = Parser.ToJSON (data);
			return Parser.getObject<T> (str);
		}
		public class ApiError{
			public string message;
		}
	}

}

