﻿using System;

namespace HauteShot.Core
{
	public interface ISettings
	{
		string UserToken {
			get;
			set;
		}
		int CurrentUserID {
			get;
			set;
		}
		void save();

		void clear();

	}
}

