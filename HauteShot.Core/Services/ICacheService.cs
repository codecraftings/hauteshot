﻿using System;

namespace HauteShot.Core
{
	public interface ICacheService
	{
		bool Has(string resourceId);
		string Get(string resourceId);
		void Save(string resourceId, string data);
		void Clear();
	}
}

