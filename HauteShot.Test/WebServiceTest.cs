﻿using NUnit.Framework;
using System;
using HauteShot.Core;
using System.IO;
using System.Text;

namespace HauteShot.Test
{
	[TestFixture ()]
	public class WebServiceTest
	{
		private IWebService webService;
		private ISettings settings;
		[SetUp ()]
		public void SetUp ()
		{
			Test.SetUp ();
			this.webService = ServiceContainer.Resolve<IWebService> ();
			this.settings = ServiceContainer.Resolve<ISettings> ();
			this.settings.UserToken = "eyJpdiI6IjV4OTBUbVlyaURzTFRBUWZKM05WRWc9PSIsInZhbHVlIjoiXC9pMzlkNzlOZU5Vb0pHOG50YjN0aGZFNFZ1blwvTEk4cEMwdmZqOTZCT0NnPSIsIm1hYyI6IjdkODE2MzMwMDM3MmE1MmY2Mjk2MjZlMzU4OTgxNjI1M2E1ODZjNDIwYWU1OTdhNTlmYzVkZDAyZDQ5N2JiZmIifQ==";
		}

		[Test ()]
		public void GetMethodTest ()
		{
			var service = webService.Create (Server.API_BASE + "feeds");
			service.SetGetParam ("app_id", "1234");
			service.SetGetParam ("access_token", Server.Instance.Access_Token);
			service.SendGet ();
			var res = service.ParseResponse<ApiResponse<Object>> ();
			Assert.That (res.status, Is.EqualTo(200));
		}

		[Test ()]
		public void PostMethodTest ()
		{
			var service = new TestWebService (Server.LOGIN_URL);
			service.SetPostParam ("app_id", "1234");
			service.SetPostParam ("username", "mahfuz");
			service.SetPostParam ("password", "bangla");
			service.SendPost ();
			var res = service.ParseResponse<ApiResponse<Object>> ();
			Assert.That (res.status, Is.EqualTo(200));
		}

		[Test ()]
		public void ImageUploadTest ()
		{
			var service = webService.Create (Server.API_BASE + "photos");
			service.SetGetParam ("app_id", "1234");
			service.SetPostParam ("access_token", Server.Instance.Access_Token);
			service.SetPostParam ("user_id", "1");
			service.SetPostParam ("caption", "test upload");
			//service.FileParams.Add ("photo", Encoding.ASCII.GetString(File.ReadAllBytes ("info.png")));
			service.SetFileParam ("photo", "info.png");
			service.SendPost ();
			var res = service.ParseResponse<ApiResponse<Object>> ();
			Assert.That (res.status, Is.EqualTo(200));
		}

		[Test ()]
		public void DeleteMethodTest ()
		{
			var service = webService.Create (Server.API_BASE + "feeds");
			service.SetGetParam ("app_id", "1234");
			service.SetGetParam ("access_token", Server.Instance.Access_Token);
			service.SendDelete ();
			var res = service.ParseResponse<ApiResponse<Object>> ();
			Assert.That (res.status, Is.EqualTo(405));
		}

		[Test ()]
		public void PutMethodTest ()
		{
			var service = webService.Create (Server.API_BASE + "users/1");
			service.SetGetParam ("app_id", "1234");
			service.SetGetParam ("access_token", Server.Instance.Access_Token);
			service.SetGetParam ("full_name", "Mahfuz Ahmed Khan");
			service.SetGetParam ("email", "mahfuz@gmail.com");
			service.SendPut ();
			var res = service.ParseResponse<ApiResponse<Object>> ();
			Assert.That (res.status, Is.EqualTo(200));
		}
	}
}

