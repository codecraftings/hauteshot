﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using HauteShot.Core;

namespace HauteShot.Test
{
	public class TestWebService:IWebService, IDisposable
	{
		private string URL;
		private Dictionary<string, string> GetParams;
		private Dictionary<string, string> PostParams;
		private Dictionary<string, string> FileParams;
		public HttpWebResponse Response;
		public static string LastResponse;
		//public Action<string> OnDataReceived;
		//public Action<int, int> OnDataSendProgress;
		//public Action<int, int> OnDataReceiveProgress;
		private readonly string contentBoundary = "-----A34B324s";

		public TestWebService(){

		}

		public TestWebService (string url)
		{
			URL = url;
			GetParams = new Dictionary<string, string> ();
			PostParams = new Dictionary<string, string> ();
			FileParams = new Dictionary<string, string> ();
		}

		public byte[] ReadImageBytes ()
		{
			throw new NotImplementedException ();
		}

		public void HandleOnProgress (float progress)
		{
			if (OnProgress != null) {
				OnProgress (progress);
			}
		}

		public Action<float> OnProgress { get; set;}

		public IWebService Create(string url){
			return new TestWebService (url);
		}

		public void SetGetParam (string key, string value)
		{
			if (GetParams != null) {
				GetParams.Add (key, value);
			}
		}

		public void SetPostParam (string key, string value)
		{
			if (PostParams != null) {
				PostParams.Add (key, value);
			}
		}

		public void SetFileParam (string key, string filepath)
		{
			if (FileParams != null) {
				FileParams.Add (key, filepath);
			}
		}

		public bool SendGet ()
		{
			var request = HttpWebRequest.Create (this.getUrl());
			request.Method = "GET";
			HttpWebResponse response;
			try{
				response = request.GetResponse() as HttpWebResponse;
			}
			catch(WebException e){
				if (e.Response == null) {
					return false;
				}
				response = e.Response as HttpWebResponse;
				//return null;
			}
			this.Response = response;
			return true;
		}

		public string ReadResponseAsString ()
		{
			using (var responseStream = Response.GetResponseStream ()) {
				var reader = new StreamReader (responseStream);
				var content = reader.ReadToEnd ();
				TestWebService.LastResponse = content;
				//Console.WriteLine (content);
				return content;
			}
		}


		public T ParseResponse<T> ()
		{
			var content = this.ReadResponseAsString ();
			return Parser.getObject<T> (content);
		}
		public bool SendPost(){
			HttpWebRequest request = HttpWebRequest.Create (this.getUrl()) as HttpWebRequest;
			request.Method = "POST";
			request.ContentType = String.Format ("multipart/form-data; boundary={0}", this.contentBoundary);
			using(var stream = request.GetRequestStream()){
				this.writePostData (stream);
				Console.WriteLine (System.DateTime.Now.Ticks);
			}
			HttpWebResponse response;
			try{
				response = request.GetResponse() as HttpWebResponse;
			}
			catch(WebException e){
				if (e.Response == null) {
					return false;
				}
				response = e.Response as HttpWebResponse;
			}
			this.Response = response;
			return true;
		}

		public bool SendDelete ()
		{
			HttpWebRequest request = HttpWebRequest.Create (this.getUrl ()) as HttpWebRequest;
			request.Method = "DELETE";
			HttpWebResponse response;
			try{
				response = request.GetResponse() as HttpWebResponse;
			}
			catch(WebException e){
				if (e.Response == null) {
					return false;
				}
				response = e.Response as HttpWebResponse;
				//return null;
			}
			this.Response = response;
			return true;
		}

		public bool SendPut()
		{
			HttpWebRequest request = HttpWebRequest.Create (this.getUrl()) as HttpWebRequest;
			request.Method = "PUT";
			HttpWebResponse response;
			try{
				response = request.GetResponse() as HttpWebResponse;
			}
			catch(WebException e){
				if (e.Response == null) {
					return false;
				}
				response = e.Response as HttpWebResponse;
			}
			this.Response = response;
			return true;
		}

		public void Dispose ()
		{
			Response.Dispose ();
			Response = null;
		}
		private string getUrl(){
			var query = this.prepareQueryParamsString ();
			return String.Format ("{0}?{1}", URL, query);
		}
		private string prepareQueryParamsString ()
		{
			var str = new StringBuilder ();
			int i = 0;
			foreach (var param in GetParams) {
				if (i == (GetParams.Count - 1)) {
					str.AppendFormat ("{0}={1}", WebUtility.UrlEncode (param.Key), WebUtility.UrlEncode (param.Value));
				} else {
					str.AppendFormat ("{0}={1}&", WebUtility.UrlEncode (param.Key), WebUtility.UrlEncode (param.Value));
				}
				i += 1;
			}
			return str.ToString ();
		}
		private void writePostData(Stream stream){
			var boundary = String.Format ("--{0}", this.contentBoundary);
			foreach (var param in PostParams) {
				writeStringLine(stream, boundary);
				writeStringLine(stream, String.Format ("Content-Disposition: form-data; name=\"{0}\"", param.Key));
				writeStringLine(stream, "");
				writeStringLine(stream, param.Value);
			}
			foreach (var param in FileParams) {
				writeStringLine(stream, boundary);
				writeStringLine(stream, String.Format ("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}.png\"", param.Key, "iphoneupload"));
				writeStringLine(stream, "Content-Type: image/png");
				writeStringLine(stream, "Content-Transfer-Encoding: binary");
				writeStringLine(stream, "");
				Console.WriteLine (System.DateTime.Now.Ticks);
				byte[] file = File.ReadAllBytes (param.Value);
				Console.WriteLine (System.DateTime.Now.Ticks);
				//var fileStream = new FileStream (param.Value, FileMode.Open);
				stream.Write (file, 0, file.Length);
				writeStringLine(stream, "");
				Console.WriteLine (System.DateTime.Now.Ticks);
			}
			writeStringLine(stream, String.Format("{0}--", boundary));
		}
		private void writeStringLine(Stream stream, string line){
			StringBuilder str = new StringBuilder ();
			str.AppendLine (line);
			byte[] buffer = Encoding.Default.GetBytes (str.ToString());
			stream.Write (buffer, 0, buffer.Length);
		}
	}
}