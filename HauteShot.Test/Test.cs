﻿using NUnit.Framework;
using System;
using HauteShot.Core;

namespace HauteShot.Test
{
	public static class Test
	{
		public static void SetUp ()
		{
			App.Instance.OnSetupTest ();
			ServiceContainer.Register<IWebService> (() => new TestWebService ());
			ServiceContainer.Register<ISettings> (() => new MockSettings ());
		}
	}
}

