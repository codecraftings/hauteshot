﻿using System;
using HauteShot.Core;

namespace HauteShot.Test
{
	public class MockSettings:ISettings
	{
		#region ISettings implementation

		public void save ()
		{

		}

		public void clear ()
		{
			UserToken = null;
			CurrentUserID = 0;
		}

		public string UserToken {
			get;
			set;
		}

		public int CurrentUserID {
			get;
			set;
		}

		#endregion

		public MockSettings ()
		{
		}
	}
}

