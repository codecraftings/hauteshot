﻿using System;
using NUnit.Framework;
using HauteShot.Core;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace HauteShot.Test
{
	[TestFixture()]
	public class ApiTest
	{
		private ISettings settings;
		[SetUp()]
		public void SetUp ()
		{
			Test.SetUp();
			settings = ServiceContainer.Resolve<ISettings> ();
			//this.settings.UserToken = "eyJpdiI6IjV4OTBUbVlyaURzTFRBUWZKM05WRWc9PSIsInZhbHVlIjoiXC9pMzlkNzlOZU5Vb0pHOG50YjN0aGZFNFZ1blwvTEk4cEMwdmZqOTZCT0NnPSIsIm1hYyI6IjdkODE2MzMwMDM3MmE1MmY2Mjk2MjZlMzU4OTgxNjI1M2E1ODZjNDIwYWU1OTdhNTlmYzVkZDAyZDQ5N2JiZmIifQ==";
		}
		[Test()]
		public void LoginTest(){
			settings.clear ();
			Assert.That (Server.Instance.Access_Token, Is.Null);
			var res = Server.Instance.Login ("mahfuz", "bangla");
			Assert.That (res, Is.True);
			Assert.That (Server.Instance.Access_Token, Is.Not.Null);
		}
		[Test()]
		public void HomeFeedTest(){
			Server.Instance.Login ("mahfuz", "bangla");
			var res = Server.Instance.GetHomeFeed ();
			Console.WriteLine (TestWebService.LastResponse);
			Console.WriteLine (res[0].ToString ());
			Assert.That (res [0].thumb_url, Is.Not.Null);
		}
		private class AA
		{
			BB dd;
			string str;
			byte[] file;
			byte[] file2;
			byte[] file3;
			string otr;
			public AA(){
				dd = new BB();
				str = "Am alive";
				otr = "otherText";
				file = File.ReadAllBytes("chrome.png");
				file2 = File.ReadAllBytes("chrome.png");
				file3 = File.ReadAllBytes("chrome.png");
			}
			public void start(){
				dd.MM (()=> Console.WriteLine(str));
			}
		}
		private class BB
		{
			public async void MM(Action col){
				Console.WriteLine ("started");
				await Task.Delay (3000);
				col.Invoke ();
			}
		}
		[Test()]
		public async void TestTest(){
			var s = new AA ();
			s.start ();
			s = null;
			Console.WriteLine(Process.GetCurrentProcess().WorkingSet64);
			GC.Collect ();
			Console.WriteLine(Process.GetCurrentProcess().WorkingSet64);
			await Task.Delay(5000);
			Console.WriteLine(GC.GetTotalMemory (false));
			//Console.WriteLine(GC.GetTotalMemory (true));
			Console.WriteLine(Process.GetCurrentProcess().WorkingSet64);
		}
	}
}

